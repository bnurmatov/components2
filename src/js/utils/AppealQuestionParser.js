var AppealQuestionParser = function () {

};
AppealQuestionParser.getAnswerAtPosition = function (userPosition, answers) {
    if (userPosition == 999) {
        var answer = answers[answers.length - 1];
        if (answer.isDistractor) {
            return answer;
        }
        return {text: ""};
    }
    for (var i = 0; i < answers.length; i++) {
        if (answers[i].position == userPosition) {
            return answers[i];
        }
    }
    return {text: ""};
};

AppealQuestionParser.parseSingleQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var userAnswers = answers[1].split(",");
    var qn = angular.copy(originQuestion);
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.SINGLE_CHOICE;
    for (var i = 0; i < userAnswers.length; i++) {
        appealQuestion.answers.push(AppealQuestionParser.getAnswerAtPosition(userAnswers[i], qn.answers));
        appealQuestion.answers[i].index = (i + 1);
        appealQuestion.userAnswers.push(angular.copy(appealQuestion.answers[i]));
        appealQuestion.userAnswers[i].selected = userAnswers[i] == answers[2];
    }
    return appealQuestion;
};

AppealQuestionParser.parseMultipleQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var answersPos = answers[1].split(",");
    var userAnswers = answers[2].split(",");
    var qn = angular.copy(originQuestion);
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.MULTIPLE_CHOICE;
    for (var i = 0; i < userAnswers.length; i++) {
        appealQuestion.answers.push(AppealQuestionParser.getAnswerAtPosition(answersPos[i], qn.answers));
        appealQuestion.answers[i].index = (i + 1);
        appealQuestion.userAnswers.push(angular.copy(appealQuestion.answers[i]));
        appealQuestion.userAnswers[i].selected = userAnswers.indexOf(answersPos[i]) !== -1;
    }
    return appealQuestion;
}
;

AppealQuestionParser.parseIndicationQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var userAnswers = answers[1].split(",");
    var uAns = answers[2].split(",");
    var qn = angular.copy(originQuestion);
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.INDICATION_TRUE_FALSE;
    for (var i = 0; i < userAnswers.length; i++) {
        appealQuestion.answers.push(AppealQuestionParser.getAnswerAtPosition(userAnswers[i], qn.answers));
        appealQuestion.answers[i].index = (i + 1);
        appealQuestion.userAnswers.push(angular.copy(appealQuestion.answers[i]));
        appealQuestion.userAnswers[i].selected = uAns[i] >= 0 ? parseInt(uAns[i], 10) : -1;
    }
    return appealQuestion;
};

AppealQuestionParser.parseEstablishQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var userAnswers = answers[1].split(",");
    var qn = angular.copy(originQuestion);
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.ESTABLISH_CONFORMITY;
    for (var i = 0; i < originQuestion.answers.length; i++) {
        var question = originQuestion.texts[i];
        appealQuestion.answers.push({
            "text": question ? question.text : '',
            "answer": originQuestion.answers[i].text
        });
        appealQuestion.answers[i].index = (i + 1);
        var usAns = isNaN(userAnswers[i]) ?
        qn.answers.find(function (item) {
            return !item.isAnswered;
        }) || {text: ''} :
            AppealQuestionParser.getAnswerAtPosition(userAnswers[i], qn.answers);
        usAns.isAnswered = true;
        if (isNaN(usAns.position)) {
            qn.answers[i].isAnswered = true;
        }
        appealQuestion.userAnswers.push({
            "text": question ? question.text : '',
            "answer": usAns.text,
            "isCorrect": !question && usAns.isDistractor ? true : (i + 1) === usAns.position
        });
    }
    return appealQuestion;
};
/*

 AppealQuestionParser.parseEstablishQuestion = function (originQuestion, userAnswer) {
 var answers = userAnswer.split("$");
 var userAnswers = answers[1].split(",");
 var qn = angular.copy(originQuestion);
 var appealQuestion = new AppealQuestionVo();
 appealQuestion.id = originQuestion.id;
 appealQuestion.title = originQuestion.text;
 appealQuestion.type = QuestionTypes.ESTABLISH_CONFORMITY;
 for (var i = 0; i < userAnswers.length; i++) {
 appealQuestion.answers.push({
 "text": originQuestion.texts[i].text,
 "answer": originQuestion.answers[i].text
 });
 appealQuestion.answers[i].index = (i + 1);
 var usAns = AppealQuestionParser.getAnswerAtPosition(userAnswers[i], qn.answers, originQuestion);
 appealQuestion.userAnswers.push({
 "text": originQuestion.texts[i].text,
 "answer": usAns.text,
 "isCorrect": (i + 1) === usAns.position
 });
 }
 return appealQuestion;
 };
 */

AppealQuestionParser.trimAnswers = function (orAnswers) {
    for (var i = 0; i < orAnswers.length; i++) {
        orAnswers[i] = orAnswers[i].trim();
    }
    return orAnswers;
};
AppealQuestionParser.parseEnterTextQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.MANUAL_ENTER_TEXT;
    appealQuestion.answers.push(originQuestion.answer.text);
    var orAnswers = originQuestion.answer.text.toLowerCase().split(',');
    orAnswers = AppealQuestionParser.trimAnswers(orAnswers);
    var userText = answers[1] !== '*' ? answers[1] : "";
    var isCorrect = userText.length === 0 ? false : orAnswers.indexOf(userText.toLowerCase()) !== -1;
    appealQuestion.userAnswers.push({text: userText, isCorrect: isCorrect});
    return appealQuestion;
};

AppealQuestionParser.parseEnterNumberQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.text;
    appealQuestion.type = QuestionTypes.MANUAL_ENTER_NUMBER;
    appealQuestion.answers.push(originQuestion.answer.text);
    var userText = answers[1] !== '*' ? answers[1] : "";
    var isCorrect = userText.length === 0 ? false : userText === originQuestion.answer.text;
    appealQuestion.userAnswers.push({text: userText, isCorrect: isCorrect});
    return appealQuestion;
};

AppealQuestionParser.parseFillingQuestion = function (originQuestion, userAnswer) {
    var answers = userAnswer.split("$");
    var userAnswers = answers[1].split(",");
    var appealQuestion = new AppealQuestionVo();
    appealQuestion.id = originQuestion.id;
    appealQuestion.title = originQuestion.title;
    appealQuestion.type = QuestionTypes.FILLING_THE_GAPS;
    var qn = angular.copy(originQuestion);
    var text = qn.text;
    text = angular.element(text);
    var selects = text.find('select');
    for (var i = 0; i < userAnswers.length; i++) {
        var elem;
        var index = parseInt(userAnswers[i], 10);
        if (index == -1) {
            elem = '<span class="filling-gap-empty"></span>';
            elem = angular.element(elem);
            elem[0].innerText = "";
        } else if (index === (i + 1)) {
            elem = '<span class="filling-gap correct"></span>';
            elem = angular.element(elem);
            elem[0].innerText = originQuestion.answers[index - 1];
        } else {
            elem = '<span class="filling-gap incorrect"></span>';
            elem = angular.element(elem);
            var answer = index == 999 && originQuestion.qs_distractor_answer ?
                originQuestion.qs_distractor_answer.text :
            originQuestion.answers[index - 1] || '';
            elem[0].innerText = answer;
        }
        angular.element(selects[i]).replaceWith(elem);
    }
    var originText = angular.element(originQuestion.text);
    selects = originText.find('select');
    for (i = 0; i < selects.length; i++) {
        elem = '<span class="filling-gap"></span>';
        elem = angular.element(elem);
        elem[0].innerText = originQuestion.answers[i];
        angular.element(selects[i]).replaceWith(elem);
    }
    appealQuestion.answers.push(originText[0].outerHTML);
    appealQuestion.userAnswers.push(text[0].outerHTML);
    return appealQuestion;
};