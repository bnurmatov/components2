function ExportUtil() {

}

ExportUtil.base64 = function (s) {
    return window.btoa(unescape(encodeURIComponent(s)))
};

ExportUtil.format = function (s, c) {
    return s.replace(/{(\w+)}/g, function (m, p) {
        return c[p];
    })
};

ExportUtil.htmlTableToExcel = function (table, name) {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><?xml version="1.0" encoding="UTF-8" standalone="yes"?><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    if (!table.nodeType) {
        table = document.getElementById(table)
    }
    table = table.cloneNode(true);
    var images = table.getElementsByTagName('img');
    for ( var i = 0; i < images.length; i++ ) {
        var parrent = images[i].parentNode;
        if(parrent) {
            parrent.removeChild(images[i]);
        }
    }
    var ctx = {worksheet: name || '', table: table.innerHTML};
    var iframe = document.createElement('iframe');
    iframe.width = "1";
    iframe.height = "1";
    iframe.frameborder = "1";
    document.body.appendChild(iframe);
    iframe.src = uri + ExportUtil.base64(ExportUtil.format(template, ctx));
    setTimeout(function() {
        document.body.removeChild(iframe);
    }, 1000);
};