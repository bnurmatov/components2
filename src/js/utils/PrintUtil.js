/**
 *
 * @constructor
 */
function PrintUtil(){
}
/**
 * Print HTML Element
 * @param elem
 */
PrintUtil.printHtmlElement = function (elem) {
    if(elem && elem.serializeWithStyles) {
        var htmlString = elem.serializeWithStyles(),
            newIframe = document.createElement('iframe');
        newIframe.width = '1px';
        newIframe.height = '1px';
        newIframe.src = 'about:blank';

        // for IE wait for the IFrame to load so we can access contentWindow.document.body
        newIframe.onload = function () {
            var script_tag = newIframe.contentWindow.document.createElement("script");
            script_tag.type = "text/javascript";
            var script = newIframe.contentWindow.document.createTextNode('function Print(){ window.focus(); window.print(); }');
            script_tag.appendChild(script);

            var cssText = "\n\nhtml, body {" +
            "\n\tmargin: 0;" +
            "\n\tpadding: 0;" +
            "\n\twidth: 100%;" +
            "\n\theight: 100%;" +
            "\n\tposition: absolute;" +
            "\n\t-webkit-box-sizing: border-box;" +
            "\n\t-moz-box-sizing: border-box;" +
            "\n\tbox-sizing: border-box; \n}";

            var head = newIframe.contentWindow.document.head;
            var sheet = newIframe.contentWindow.document.createElement('style');
            sheet.innerHTML = cssText;
            sheet.type = "text/css";
            newIframe.contentWindow.document.body.appendChild(sheet);
            //head.append(sheet);
            newIframe.contentWindow.document.body.innerHTML = htmlString;
            newIframe.contentWindow.document.body.appendChild(script_tag);

            // for chrome, a timeout for loading large amounts of content
            setTimeout(function () {
                newIframe.contentWindow.Print();
                newIframe.contentWindow.document.body.removeChild(script_tag);
                newIframe.parentElement.removeChild(newIframe);
            }, 200);
        };
        document.body.appendChild(newIframe);
    }
};