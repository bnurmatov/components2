/**
 * Utility class provides information about current environment.
 * @constructor
 */
function PlatformUtil() {

}

/**
 * Is Mac OS X
 * @return {boolean}
 */
PlatformUtil.isMac = function () {
    return window.navigator.platform.toUpperCase().indexOf('MAC') >= 0;
};


/**
 * Is Windows
 * @return {boolean}
 */
PlatformUtil.isWindows = function () {
    return window.navigator.platform.toUpperCase().indexOf('WIN32') >= 0;
};

/**
 * Is Android device
 * @return {boolean}
 */
PlatformUtil.isAndroid = function () {
    return navigator.userAgent.toLowerCase().indexOf('android') !== -1;
};

/**
 * Is iOS device (iPad, iPhone, iPod)
 * @return {boolean}
 */
PlatformUtil.isAndroidWebView = function () {
    var isWebView = false;
    if (PlatformUtil.isAndroid() && /AppName\/[0-9\.]+$/.test(window.navigator.userAgent)) {
        isWebView = true;
    }
    return isWebView;
};
/**
 * Is iOS device (iPad, iPhone, iPod)
 * @return {boolean}
 */
PlatformUtil.isIOS = function () {
    return /(iPhone|iPod|iPad).*AppleWebKit.*(?!.*Safari)/i.test(window.navigator.userAgent);
};

/**
 * Is iOS device (iPad, iPhone, iPod)
 * @return {boolean}
 */
PlatformUtil.isIOSWebView = function () {
    var standalone = window.navigator.standalone,
        isWebView = false;
    if (PlatformUtil.isIOS() && !standalone && !PlatformUtil.isSafari()) {
        isWebView = true;
    }
    return isWebView;
};
/**
 * Is a mobile device (iOS or Android)
 * @return {boolean}
 */
PlatformUtil.isMobile = function () {
    return PlatformUtil.isAndroid() || PlatformUtil.isIOS();
};
/**
 * Is a mobile device (iOS or Android)
 * @return {boolean}
 */
PlatformUtil.isMobileNativeApp = function () {
    return PlatformUtil.isAndroidWebView() || PlatformUtil.isIOSWebView();
};
/**
 * Is Opera browser
 * @return {boolean}
 */
PlatformUtil.isOpera = function () {
    return !!window.opera || window.navigator.userAgent.indexOf('Opera') >= 0;
};

/**
 * Is Firefox browser
 * @return {boolean}
 */
PlatformUtil.isFirefox = function () {
    return typeof InstallTrigger !== 'undefined';
};

/**
 * Is Internet Explorer browser
 * @return {boolean}
 */
PlatformUtil.isIE = function () {
    return typeof document.documentMode !== "undefined";
};


/**
 * Is Safari browser
 * @return {boolean}
 */
PlatformUtil.isSafari = function () {
    return Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
};

/**
 * Is Chrome browser
 * @return {boolean}
 */
PlatformUtil.isChrome = function () {
    return !!window.chrome;
};