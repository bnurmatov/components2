var ExaminationTypes = function () {

};

ExaminationTypes.Types = [
    'Simple',
    'Рейтинги 1',
    'Рейтинги 2',
    'Имтиҳони Омӯзгор',
    'Имтиҳони Ҷамбастӣ',
    'Имтиҳони Триместр'
];

ExaminationTypes.getExaminationType = function (index) {
    return ExaminationTypes.Types[index];
};