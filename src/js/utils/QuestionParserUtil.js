/**
 *
 * @constructor
 */
function QuestionParserUtil() {

}

/**
 * Parse single question
 * @param {Object} item
 * @returns {QuestionVO}
 */
QuestionParserUtil.parseQuestion = function (item) {
    var vo = new QuestionVO(),
        correctAnswers = item.qs_corrects ? item.qs_corrects.split(',').map(function (item) {
            return parseInt(item, 10)
        }) : [];

    vo.qs_id = parseInt(item.qs_id, 10);
    vo.test_id = parseInt(item.test_id, 10);
    vo.reyting = parseInt(item.reyting, 10);
    vo.qs_type = parseInt(item.qs_type, 10);
    vo.qs_group = parseInt(item.qs_group, 10);
    vo.qs_title = item.qs_title;
    vo.qs_text1 = item.qs_text1;
    vo.qs_text2 = item.qs_text2;
    vo.qs_text3 = item.qs_text3;
    vo.qs_text4 = item.qs_text4;
    vo.qs_text5 = item.qs_text5;
    vo.qs_answer1 = new AnswerVO(item.qs_answer1, correctAnswers.indexOf(1) !== -1);
    vo.qs_answer2 = new AnswerVO(item.qs_answer2, correctAnswers.indexOf(2) !== -1);
    vo.qs_answer3 = new AnswerVO(item.qs_answer3, correctAnswers.indexOf(3) !== -1);
    vo.qs_answer4 = new AnswerVO(item.qs_answer4, correctAnswers.indexOf(4) !== -1);
    vo.qs_answer5 = new AnswerVO(item.qs_answer5, correctAnswers.indexOf(5) !== -1);
    vo.qs_distractor_answer = new AnswerVO(item.qs_distractor_answer, false);
    vo.qs_blocked = parseInt(item.qs_blocked, 10) != 0;
    vo.qs_invalid = parseInt(item.qs_invalid, 10) != 0;
    vo.qs_comment = item.qs_comment;
    vo.qs_corrects = item.qs_corrects;
    return vo;
};

/**
 * Parse questions from json string
 * @param jsonStr
 */
QuestionParserUtil.parseQuestions = function (questions) {
    var len = questions.length,
        arr = [];
    for (var i = 0; i < len; i++) {
        arr.push(QuestionParserUtil.parseQuestion(questions[i]));
    }
    return arr;
};

/**
 *
 * @param question
 * @returns {SingleVO}
 */
QuestionParserUtil.parseSingle = function ($sce, question) {
    var qs = new SingleVO();
    var answer;
    var correctID;
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.text = $sce.trustAsHtml(question.qs_text1);
    qs.answers = [];
    correctID = parseInt(question.qs_corrects, 10) - 1;
    for (var i = 0; i < 5; i++) {
        if (question['qs_answer' + (i + 1)] && question['qs_answer' + (i + 1)].text) {
            answer = new AnswerVO();
            answer.position = (i + 1);
            answer.text = $sce.trustAsHtml(question['qs_answer' + (i + 1)].text);
            answer.hashSumma = correctID === i ? Base64.encode("1#" + Date.now() + "#0") : Base64.encode("0#" + Date.now() + "#1");
            answer.isCorrect = correctID === i;
            qs.answers.push(answer);
        }
    }
    return qs;
};

/**
 *
 * @param question
 * @returns {MultipleVO}
 */
QuestionParserUtil.parseMultiple = function ($sce, question) {
    var qs = new MultipleVO();
    var correctIDs;
    var answer;
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.text = question.qs_text1.replace(/tst-multiple-answers-holder/gmi, "simple");
    qs.text = $sce.trustAsHtml(qs.text);
    correctIDs = question.qs_corrects.split(',');
    for (var i = 0; i < correctIDs.length; i++) {
        correctIDs[i] = parseInt(correctIDs[i], 10) - 1;
    }
    qs.answers = [];
    for (var i = 0; i < 5; i++) {
        if (question['qs_answer' + (i + 1)] && question['qs_answer' + (i + 1)].text) {
            answer = new AnswerVO();
            answer.position = (i + 1);
            answer.text = $sce.trustAsHtml(question['qs_answer' + (i + 1)].text);
            answer.hashSumma = correctIDs.indexOf(i) !== -1 ? Base64.encode("1#" + Date.now() + "#0") : Base64.encode("0#" + Date.now() + "#1");
            answer.isCorrect = correctIDs.indexOf(i) !== -1;
            qs.answers.push(answer);
        }
    }
    return qs;
};


/**
 *
 * @param question
 * @returns {EstablishVO}
 */
QuestionParserUtil.parseEstablish = function ($sce, question) {
    var qs = new EstablishVO();
    var answer, content, images, text;
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.text = $sce.trustAsHtml(question.qs_title);
    qs.texts = [];
    for (var i = 0; i < 5; i++) {
        if (question['qs_text' + (i + 1)] && question['qs_text' + (i + 1)].length > 0) {
            content = angular.element(question['qs_text' + (i + 1)]);
            images = content.find('img');
            text = content.text().trim();
            if (!text && (!images || images.length == 0)) {
                continue;
            } else {
                qs.texts.push({"position": (i + 1), "text": question['qs_text' + (i + 1)]})
            }
        }
    }
    qs.answers = [];
    for (var i = 0; i < 5; i++) {
        if (question['qs_answer' + (i + 1)] && question['qs_answer' + (i + 1)].text) {
            content = angular.element(question['qs_answer' + (i + 1)].text);
            images = content.find('img');
            text = content.text().trim();
            if (!text && (!images || images.length == 0)) {
                continue;
            } else {
                answer = new AnswerVO();
                answer.position = (i + 1);
                answer.text = question['qs_answer' + (i + 1)].text;
                answer.hashSumma = Base64.encode("1#" + Date.now() + "#0");
                qs.answers.push(answer);
            }
        }
    }
    if (question.qs_distractor_answer && question.qs_distractor_answer.text) {
        answer = new AnswerVO();
        answer.isDistractor = true;
        answer.position = qs.answers.length + 1;
        answer.text = question.qs_distractor_answer.text;
        answer.hashSumma = Base64.encode("1#" + Date.now() + "#0");
        qs.answers.push(answer);
    }
    return qs;
};


/**
 *
 * @param question
 * @returns {IndicationVO}
 */
QuestionParserUtil.parseIndication = function ($sce, question) {
    var qs = new IndicationVO();
    var answer;
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    question.qs_title = question.qs_title.replace(/tst-indication-answers-holder/gmi, "simple");
    qs.text = $sce.trustAsHtml(question.qs_title);
    qs.answers = [];
    for (var i = 0; i < 5; i++) {
        if (question['qs_text' + (i + 1)] && question['qs_text' + (i + 1)].length > 0) {
            answer = new AnswerVO();
            answer.position = (i + 1);
            answer.text = $sce.trustAsHtml(question['qs_text' + (i + 1)]);
            answer.isCorrect = question['qs_answer' + (i + 1)].text == 1;
            answer.hashSumma = question['qs_answer' + (i + 1)].text == 1 ? Base64.encode("1#" + Date.now() + "#0") : Base64.encode("0#" + Date.now() + "#1");
            qs.answers.push(answer);
        }
    }

    return qs;
};

/**
 *
 * @param question
 * @returns {EnterNumberVO}
 */
QuestionParserUtil.parseEnterNumber = function ($sce, question) {
    var qs = new EnterNumberVO();
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.text = $sce.trustAsHtml(question.qs_text1);
    qs.answer = question.qs_answer1;
    return qs;
};

/**
 *
 * @param question
 * @returns {EnterTextVO}
 */
QuestionParserUtil.parseEnterText = function ($sce, question) {
    var qs = new EnterTextVO();
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.text = $sce.trustAsHtml(question.qs_text1);
    qs.answer = question.qs_answer1;
    return qs;
};

/**
 *
 * @param question
 * @returns {FillingGapsVO}
 */
QuestionParserUtil.parseFillingGaps = function (question) {
    var qs = new FillingGapsVO();
    qs.id = question.qs_id;
    qs.type = question.qs_type;
    qs.title = question.qs_title;
    qs.text = question.qs_text1;
    qs.answers = [];
    for (var i = 0; i < 5; i++) {
        if (question['qs_answer' + (i + 1)] && question['qs_answer' + (i + 1)].text && question['qs_answer' + (i + 1)].text !== "null") {
            qs.answers.push(question['qs_answer' + (i + 1)].text)
        }
    }
    if (question.qs_distractor_answer && question.qs_distractor_answer.text && question.qs_distractor_answer.text !== "null") {
        qs.answers.push(question.qs_distractor_answer.text);
        qs.qs_distractor_answer = question.qs_distractor_answer;
    }
    var formated = QuestionParserUtil.formatSentence(qs);
    qs.text = formated.text;
    qs.answers = formated.answers;
    return qs;
};

QuestionParserUtil.formatSentence = function (question) {
    var ansLen,
        answerIndexs = [],
        randomazed = [],
        corrects = [],
        formattedString,
        index,
        str;

    formattedString = "<div>";
    randomazed = ArrayUtil.randomize(angular.copy(question.answers));
    for (var i = 0; i < question.answers.length; i++) {
        index = question.text.indexOf(question.answers[i]);
        if (index === -1 && question.qs_distractor_answer) {
            index = 999;
        }
        if (index !== -1) {
            answerIndexs.push({'index': index, 'answer': question.answers[i]});
        }
    }

    answerIndexs.sort(QuestionParserUtil.sortByIndex);
    ansLen = answerIndexs.length - (question.qs_distractor_answer ? 1 : 0);
    str = question.text.substring(0, answerIndexs[0].index);
    formattedString += str;
    for (i = 0; i < ansLen; i++) {
        formattedString += '<select class="tst-filling-gap-option"><option ng-show="false" selected disabled value=""></option>';
        randomazed = ArrayUtil.randomize(randomazed);
        for (var j = 0; j < answerIndexs.length; j++) {
            formattedString += "<option value='" + randomazed[j] + "'>" + randomazed[j] + "</option>";
        }
        formattedString += '</select>';
        str = question.text.substr(answerIndexs[i].index, answerIndexs[i].answer.length);
        corrects.push(str);
        if (i === (ansLen - 1)) {
            str = question.text.substring(answerIndexs[i].index + answerIndexs[i].answer.length, question.text.length);
            formattedString += str;
        } else {
            str = question.text.substring(answerIndexs[i].index + answerIndexs[i].answer.length, answerIndexs[i + 1].index);
            formattedString += str;
        }
    }
    formattedString += "</div>";
    return {'text': formattedString, 'answers': corrects};
};

/**
 * Sorts array objects by index property
 * @param a
 * @param b
 * @returns {number}
 */
QuestionParserUtil.sortByIndex = function (a, b) {
    if (a.index < b.index) {
        return -1;
    }
    if (a.index > b.index) {
        return 1
    }
    return 0;
};