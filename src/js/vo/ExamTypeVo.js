/**
 *
 * @constructor
 */
var ExamTypeVo = function () {

    /**
     *
     * @type {null}
     */
    this.id = null;

    /**
     *
     * @type {null}
     */
    this.name = null;

    /**
     *
     * @type {null}
     */
    this.variants = null;
};