/**
 * Created by Komron Musoev on 9/18/14.
 */

angular.module('singleQuestion', [])
    .directive('singleQuestion', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                singleVo: '=?',
                number: '@?',
                addQuestions: '&?'
            },
            template: '<div class="tst-single-question">\n    <div style="display: table;">\n        <span style="display: table-cell;" class="tst-single-question-text">{{number}}.</span>\n        <span style="display: table-cell;" ng-bind-html="singleVo.text"></span>\n    </div>\n    <div style="display: table;" class="tst-single-answers-holder" ng-repeat="answer in singleVo.answers">\n        <span style="display: table-cell;">{{$index + 1}}.</span>\n        <input style="display: table-cell;" type="radio" ng-model="$parent.selected" value="{{answer.position}}"/>\n        <span style="display: table-cell;" ng-bind-html="answer.text"></span>\n    </div>\n</div>',
            link: function (scope, element, attrs) {

                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                var watchers = [];

                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;

                    watchers = null;
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.addQuestions({questionScope: scope});
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------

                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);
