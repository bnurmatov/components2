angular.module('treeViewItem', [])
    .directive("treeViewItem", ['$rootScope', '$compile', 'locale', function ($rootScope, $compile, locale) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                menuVo: '=?'
            },
            template: '<li class="tree-view-item">\n   <i class="tree-view-item-collapsed" ng-class="{\'tree-view-item-expanded\' : !menuVo.collapsed}" ng-show="menuVo.children.length > 0" ng-click="expandCollapseItem(menuVo)" title="{{menuVo.collapsed ? locale.expandButtonTooltip : locale.collapseButtonTooltip}}"></i>\n   <i class="tree-view-item-normal" ng-show="!menuVo.children || menuVo.children.length === 0"></i> \n   <span ng-show="menuVo.children.length > 0" ng-class="menuVo.selected" ng-bind="menuVo.label" ng-click="expandCollapseItem(menuVo, true)" title="{{locale.selectItemTooltip}}"></span>\n   <span ng-show="!menuVo.children || menuVo.children.length === 0" ng-class="menuVo.selected" ng-click="itemClicked(menuVo)" ng-bind="menuVo.label" title="{{locale.selectItemTooltip}}"></span>\n</li>\n',
            link: function (scope, element) {
                //---------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------

                var watchers = [];
                //---------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------


                //---------------------------------------------------------
                // Private Methods
                //---------------------------------------------------------
                /**
                 * Clean up
                 * @param event
                 */
                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    onDeselectItem = null;
                    onMenuVoChanghed = null;
                    resourceChanged = null;

                    watchers = null;
                }
                /**
                 * Deselect all items
                 * @param newValue
                 * @param oldValue
                 */
                function onDeselectItem(event, item) {
                    if (scope.menuVo && scope.menuVo !== item) {
                        scope.menuVo.selected = null;
                        $rootScope.$$phase || scope.$apply();
                    }
                }
                /**
                 * Item change handler
                 * @param newValue
                 * @param oldValue
                 */
                function onMenuVoChanghed(newValue, oldValue){
                    if(newValue && newValue.children && newValue.children.length > 0){
                        var template = '<tree-view tree-model="menuVo.children" is-visible="!menuVo.collapsed"></tree-view>';
                        element.append( $compile( template )( scope ) );
                    }
                }
                /**
                 * Locale file loaded handler
                 */
                function resourceChanged() {
                    scope.locale = locale.props;
                    $rootScope.$$phase || scope.$apply();
                }
                //---------------------------------------------------------
                // Public Methods
                //---------------------------------------------------------
                /**
                 * Expand or collapse menu item
                 * @param selectedNode
                 */
                scope.expandCollapseItem = function (selectedNode, isCourse) {
                    selectedNode.collapsed = !selectedNode.collapsed;
                    $rootScope.$$phase || scope.$apply();
                    if(isCourse){
                        $rootScope.$emit('treeItemCollapseChanged', selectedNode.course);
                    }
                    if(isCourse && !selectedNode.collapsed){
                        $rootScope.$emit('courseSelected', selectedNode.course);
                    }
                };
                /**
                 *
                 * @param selectedNode
                 */
                scope.itemClicked = function (selectedNode) {
                    selectedNode.selected = 'tree-view-item-selected';
                    $rootScope.$$phase || scope.$apply();
                    $rootScope.$emit('treeMenuSelectionChange', scope.menuVo.course, scope.menuVo.label);
                    $rootScope.$emit('deselectItem', scope.menuVo);

                };

                /**
                 *
                 * Attach event listeners
                 */
                watchers.push(scope.$watch('menuVo', onMenuVoChanghed));
                watchers.push($rootScope.$on('localeLoaded', resourceChanged));
                watchers.push($rootScope.$on('deselectItem', onDeselectItem));
                watchers.push(scope.$on('$destroy', onDestroy));
                /**
                 * We need to call method after declaration because
                 * maybe our component can initialized after loading config from server
                 */
                resourceChanged();
            }
        };
    }]);