angular.module('editVedemostView', [])
    .directive('editVedemostView', ['$rootScope', '$interval', 'VedemostService', function ($rootScope, $interval, VedemostService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isVisible: '=?',
                isFullAccess: '=?',
                isTrimester: '=?'
            },
            template: '<div class="vedemost-question-view">\n    <div class="vedemost-answers-container">\n        <table class="vedemost-main-table">\n            <tbody>\n            <tr>\n                <td class="vedemost-cl0"><span><strong>№</strong></span></td>\n                <td style="width: 42%;"><span><strong>Номи Насаб</strong></span></td>\n\n                <td style="width: 16%;text-align: center;"><span><strong>Амалиёт</strong></span></td>\n            </tr>\n            <tr ng-repeat="student in students">\n                <td class="vedemost-cl0">{{$index+1}}</td>\n                <td style="width: 42%;" ng-bind="student.FioTj"></td>\n                <td style="width: 16%;text-align: center;">\n                    <img class="vedemost-lock-btn" src="img/unlock-icon.png" ng-show="student.st_status == 0"\n                        />\n                    \n                    <img class="vedemost-lock-btn" src="img/work_in_progress.png" ng-show="student.st_status == 1"\n                         ng-click="onUnlockInprogressStudent(student)" />\n                    \n                    <img class="vedemost-lock-btn" \n                         src="img/lock-icon.png" \n                         ng-show="student.st_status == 4"\n                         ng-class="{\'disable-btn\': student.score > 0}"\n                         ng-click="onUnlockTestButtonClickHandler(student)" />\n                    <a href="" ng-click="onRemoveVariant(student)" ng-show="student.st_status == 0">Несткунии Вариант</a>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n    <div class="vedemost-view-footer">\n        <button class="cw-button cw-blue-medium-button" ng-click="onReturnClicked()"><span>БОЗГАШТ</span></button>\n    </div>\n</div>\n',
            link: function (scope) {
                var watchers = [], timerId;

                scope.students = null;
                scope.currentStudent = null;

                function onDestroy(){
                    if(timerId){
                        $interval.cancel(timerId);
                        timerId = null;
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }

                function retrieveStudentsStatus(){
                    if(timerId){
                        $interval.cancel(timerId);
                        timerId = null;
                    }
                    if(scope.isTrimester){
                        VedemostService.getTrimesterStudentsByVedemostId(VedemostService.currentVedemost.vd_id);
                    } else {
                        VedemostService.getStudentsByVedemostId(VedemostService.currentVedemost.vd_id);
                    }
                    timerId = $interval(retrieveStudentsStatus, 60000);
                }

                function onIsVisibleChange(newValue, oldValue){
                    if(newValue != oldValue){
                        if(newValue) {
                            timerId = $interval(retrieveStudentsStatus, 200);
                        } else {
                            if(timerId){
                                $interval.cancel(timerId);
                                timerId = null;
                            }
                        }
                    }
                }

                function onPolugodieChange(event, polugodie){
                    scope.polugodie = polugodie;
                    if(scope.isVisible){
                        scope.onReturnClicked();
                    }
                }

                function onStudentsLoaded(){
                    if(scope.isVisible) {
                        scope.students = VedemostService.students;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                function onDeleteVariantConfirm(){
                    if(scope.isVisible && scope.currentStudent && VedemostService.currentVedemost){
                        VedemostService.removeStudentVariant(scope.currentStudent.vd_stud_id, VedemostService.currentVedemost.vd_id,  VedemostService.currentVedemost.exam_type, scope.polugodie);
                    }
                }

                function onStudentVariantRemoved(){

                }

                function onStudentStatusUpdated(){
                    if(scope.isVisible) {
                        scope.currentStudent.st_status = 0;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                scope.onReturnClicked = function(){
                    $rootScope.$emit('showVedemostView');
                };

                scope.onUnlockInprogressStudent = function(student){
                    if(scope.isVisible) {
                        scope.currentStudent = student;
                        VedemostService.updateStudentVedemostStatus(student.vd_id, student.vd_stud_id, 0, student.exam_type);
                    }
                };

                scope.onUnlockTestButtonClickHandler = function(student){
                    if(+student.score > 0){
                        return;
                    }
                    if(scope.isFullAccess) {
                        scope.currentStudent = student;
                        VedemostService.updateStudentVedemostStatus(student.vd_id, student.vd_stud_id, 0, student.exam_type);
                    } else {
                        $rootScope.$emit('open_ng_popup', 'infoPopup', 'Ба сардори офиси бақайдгиранда муроҷиат намоед.');
                    }
                };

                scope.onRemoveVariant = function(student){
                    scope.currentStudent = student;
                    $rootScope.$emit('open_ng_popup', 'deleteVariantPopup');
                };

                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push($rootScope.$on('studentsLoaded', onStudentsLoaded));
                watchers.push($rootScope.$on('studentVariantRemoved', onStudentVariantRemoved));
                watchers.push($rootScope.$on('deleteVariantConfirm', onDeleteVariantConfirm));
                watchers.push($rootScope.$on('studentStatusUpdated', onStudentStatusUpdated));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);