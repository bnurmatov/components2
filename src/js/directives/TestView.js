angular.module('testModule', ['enterNumber', 'enterText', 'establishConfirmity', 'fillingGaps', 'indicationTrueFalse', 'multipleQuestion', 'singleQuestion'])
    .directive('testingView', ["$compile", "$rootScope", "$interval",
        function ($compile, $rootScope, $interval) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    questions: '=?'
                },
                controller: function ($scope) {

                    //---------------------------------------------------------------------
                    // Private Variables.
                    //---------------------------------------------------------------------

                    //---------------------------------------------------------------------
                    // Public Variables.
                    //---------------------------------------------------------------------

                    /**
                     *
                     * @type {Array}
                     */
                    $scope.questionElements = [];

                    /**
                     *
                     * @type {Array}
                     */
                    $scope.questionScopes = [];

                    //---------------------------------------------------------------------
                    // Private Functions.
                    //---------------------------------------------------------------------


                    //---------------------------------------------------------------------
                    // Public Functions.
                    //---------------------------------------------------------------------

                    $scope.addQuestions = function (questionScope) {
                        var id = parseInt(questionScope.number, 10) - 1;
                        $scope.questionScopes[id] = questionScope;
                        $scope.questionElements[id] = questionScope.questionElement;
                    }

                },
                template: '<div>\n    <div class="tst-container"></div>\n</div>',
                link: function (scope, element, attr) {

                    //-------------------------------------------------------------------
                    // Private Variables
                    //-------------------------------------------------------------------

                    var watchers = [],

                        /**
                         *
                         * @type {Array}
                         */
                        establishs = [],

                        /**
                         * Property to hold items container of directive.
                         * @type {*}
                         */
                        itemsContainer = null,

                        /**
                         *
                         */
                        interval;


                    itemsContainer = angular.element(element[0].getElementsByClassName('tst-container')[0]);
                    //-------------------------------------------------------------------
                    // Public Variables
                    //-------------------------------------------------------------------

                    //-------------------------------------------------------------------
                    // Private Functions
                    //-------------------------------------------------------------------

                    function onEstablishChecked(event, data) {
                        if (data) {
                            establishs.push(data);
                        }
                    }

                    function onConfirmEventHandler(event, data) {
                        if (data) {
                            $rootScope.$emit('check_establish');
                            checkUserAnswers();
                        }
                    }

                    function onRemoveComplete() {
                        itemsContainer.empty();
                        establishs = [];
                        $rootScope.$$phase || scope.$apply();
                    }

                    function onDestroy() {
                        while (watchers.length > 0) {
                            watchers.shift()();
                        }

                        for (var key in scope) {
                            if (key[0] !== '$') {
                                scope[key] = null;
                            }
                        }
                        onEstablishChecked = null;
                        onConfirmEventHandler = null;
                        onRemoveComplete = null;
                        onDestroy = null;
                        onStartTesting = null;
                        positionQuestions = null;
                        checkUserAnswers = null;
                    }


                    function onStartTesting() {
                        if (scope.questions && scope.questions.length > 0) {
                            positionQuestions();
                        }
                    }

                    /**
                     *
                     */
                    function positionQuestions() {
                        var questionTag,
                            drvQuestion,
                            questionLen,
                            index = 1;

                        itemsContainer.empty();
                        questionLen = scope.questions.length;
                        for (var i = 0; i < questionLen; i++) {
                            switch (scope.questions[i].type) {
                                case 1://QuestionTypes.SINGLE_CHOICE:
                                    questionTag = '<single-question number="' + index + '" single-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)">';
                                    break;
                                case 2://QuestionTypes.MULTIPLE_CHOICE:
                                    questionTag = '<multiple-question number="' + index + '" multiple-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)">';
                                    break;
                                case 5://QuestionTypes.ESTABLISH_CONFORMITY:
                                    questionTag = '<establish-container number="' + index + '" establish-vo="questions[' + i + ']">';
                                    break;
                                case 6://QuestionTypes.INDICATION_TRUE_FALSE:
                                    questionTag = '<indication-true-false config="config" number="' + index + '" indication-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)">';
                                    break;
                                case 4://QuestionTypes.MANUAL_ENTER_NUMBER:
                                    questionTag = '<enter-number number="' + index + '" enter-number-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)">';
                                    break;
                                case 3://QuestionTypes.MANUAL_ENTER_TEXT:
                                    questionTag = '<enter-text show-answer="1" number="' + index + '" enter-text-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)" >';
                                    break;
                                case 7://QuestionTypes.FILLING_THE_GAPS:
                                    questionTag = '<filling-gaps number="' + index + '" filling-vo="questions[' + i + ']" add-questions="addQuestions(questionScope)">';
                                    break;
                            }
                            drvQuestion = angular.element(questionTag);
                            itemsContainer.append($compile(drvQuestion)(scope));
                            itemsContainer.append($compile(angular.element('<hr/>'))(scope));
                            index++;
                        }
                    }

                    /**
                     *
                     */
                    function checkUserAnswers() {
                        var question,
                            userQuestion,
                            questionsLen,
                            scoreVO,
                            answerVOS,
                            answer;

                        scoreVO = new ScoreVo();
                        answerVOS = [];
                        questionsLen = scope.questionElements.length;
                        for (var i = 0; i < questionsLen; i++) {
                            question = scope.questions[i];
                            userQuestion = scope.questionElements[i];
                            switch (question.type) {
                                case QuestionTypes.SINGLE_CHOICE:
                                    answer = QuestionCheckerUtil.checkSingleQuestion(question, userQuestion);
                                    break;
                                case QuestionTypes.MULTIPLE_CHOICE:
                                    answer = QuestionCheckerUtil.checkMultipleQuestion(question, userQuestion);
                                    break;
                                case QuestionTypes.ESTABLISH_CONFORMITY:
                                    //answer = checkEstablishQuestion(question, userQuestion);
                                    answer = establishs.shift();
                                    break;
                                case QuestionTypes.INDICATION_TRUE_FALSE:
                                    answer = QuestionCheckerUtil.checkIndicationQuestion(question, userQuestion);
                                    break;
                                case QuestionTypes.MANUAL_ENTER_NUMBER:
                                    answer = QuestionCheckerUtil.checkInterNumberQuestion(question, userQuestion);
                                    break;
                                case QuestionTypes.MANUAL_ENTER_TEXT:
                                    answer = QuestionCheckerUtil.checkInterTextQuestion(question, userQuestion);
                                    break;
                                case QuestionTypes.FILLING_THE_GAPS:
                                    answer = QuestionCheckerUtil.checkFillingGapsQuestion(question, userQuestion);
                                    break;
                            }
                            scoreVO.corrects += answer.corrects;
                            scoreVO.userAnswers.push(answer.answerVO);
                            answerVOS = answerVOS.concat(answer.answerVO.userAnswers);
                        }
                        scoreVO.incorrects = scoreVO.total - scoreVO.corrects;
                        $rootScope.$emit('open_ng_popup', 'testingResult');
                    }

                    function onQuestionsChanged(newValue){
                        if(newValue){
                            positionQuestions();
                        }
                    }
                    //-------------------------------------------------------------------
                    // Public Functions
                    //-------------------------------------------------------------------

                    //-------------------------------------------------------------------
                    // Watchers (Event Listeners)
                    //-------------------------------------------------------------------

                    watchers.push(scope.$watch('questions', onQuestionsChanged));
                    watchers.push(scope.$on('$destroy', onDestroy));
                    watchers.push($rootScope.$on('establish_checked', onEstablishChecked));
                    watchers.push($rootScope.$on('confirm_complete_event', onConfirmEventHandler));
                    watchers.push($rootScope.$on('remove_complete_popup', onRemoveComplete));

                    onStartTesting();
                }
            }
        }])
;