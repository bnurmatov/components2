/**
 * Created by Komron Musoev on 9/18/14.
 */
angular.module('establishConfirmity', [])
    .directive('establishContainer', ['$rootScope', '$window', '$compile', '$sce', function ($rootScope, $window, $compile, $sce) {
        /**
         * 20px  place from left side of dropTarget
         * 100px between dragTarget and dropTarget and
         * 20px place from right side of dragTarget
         * @type {number}
         */
        var EMPTY_PLACE = 100,

            /**
             * 20px  empty place between
             * left side of droptarget
             * and drag container
             * @type {number}
             */
            LEFT_DROPTARGET = 30,

            /**
             *
             * @type {number}
             */
            LEFT_DRAGTARGET = 70,


            /**
             * right space from dragtarget
             * when it connected to droptarget
             * @type {number}
             */
            RIGHT_DRAG = 4,

            /**
             * right space  from  dragtarget
             * when it not connected to droptarget
             * @type {number}
             */
            RIGHT_DRAG_WITH_SPACE = 70;

        return {
            restrict: 'E',
            replace: true,
            template: '<div class="tst-establish-question">\n    <span>{{number}}. </span>\n\n    <div class="tst-establish-title" ng-bind-html="establishVo.text"></div>\n    <div class="est-items-container"></div>\n</div>',
            scope: {
                number: '@?',
                establishVo: '=?'
            },
            controller: function ($scope) {

                //---------------------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------------------

                /**
                 * Array of drag elements.
                 * @type {Array}
                 */
                $scope.dragels = [];

                /**
                 * Array of drag scopes.
                 * @type {Array}
                 */
                $scope.drags = [];

                /**
                 * Array of drop elements.
                 * @type {Array}
                 */
                $scope.dropels = [];


                /**
                 * Array of drop scopes.
                 * @type {Array}
                 */
                $scope.drops = [];

                //---------------------------------------------------------------------
                // Private functions
                //---------------------------------------------------------------------

                /**
                 * Function that find specific drag  item  at  y  point of document.
                 *  @param {Number} y
                 *  @return {Int}
                 */
                function findElementIndexAtYPoint(y) {
                    for (var i = 0; i < $scope.drags.length; i++) {
                        var drag = $scope.drags[i];
                        drag.top = parseInt(drag.top, 10);
                        if (drag.top === y) {
                            return drag.id;
                        }
                    }
                    return -1;
                }

                /**
                 *  Moves element to original position
                 * @param {JQLite} dragElement drag element
                 * @param {Object} dragScope Isolated scope of drag item
                 */
                function moveToOriginalPosition(dragScope, dragElement) {
                    if (dragScope.dropId >= 0) {
                        var dropScope = $scope.drops[dragScope.dropId];
                        dropScope.isDragged = false;
                        dropScope.dragId = -1;
                        dropScope.sortDrags = null;
                        dragScope.dropId = -1;
                    }
                    TweenMax.fromTo(dragElement, 0.5, {
                        left: dragElement.prop('offsetLeft'),
                        top: dragElement.prop('offsetTop')
                    }, {
                        left: dragScope.left, top: dragScope.top, onComplete: function () {
                            dragElement.css('zIndex', 0);
                        }
                    });
                }

                //---------------------------------------------------------------------
                // Scope Functions
                //---------------------------------------------------------------------

                /**
                 * Called from drag item
                 * @param {JQLite} dragElement drag element
                 * @param {Object} dragScope Isolated scope of drag item
                 */
                $scope.addDrag = function (dragScope) {
                    dragScope.dropId = -1;
                    $scope.drags[dragScope.id] = dragScope;
                    $scope.dragels[dragScope.id] = dragScope.dragElement;
                };

                /**
                 * Called from drop item
                 * @param {JQLite} dropElement drop element
                 * @param {Object} dropScope Isolated scope of drop item
                 */
                $scope.addDrop = function (dropScope) {
                    dropScope.isDragged = false;
                    $scope.drops[dropScope.id] = dropScope;
                    $scope.dropels[dropScope.id] = dropScope.dropElement;
                };

                /**
                 * Drag item to new  point.
                 * @param {Number} x
                 * @param {Number} y
                 * @param {Number} newY
                 */
                $scope.onDragItemCall = function (x, y, newY, dragScope) {
                    var index = findElementIndexAtYPoint(y);
                    if (index !== -1) {
                        var drag = $scope.drags[index];
                        var el = $scope.dragels[index];
                        if (drag != dragScope && drag.dropId !== -1) {
                            drag.dropId = -1;
                        }
                        drag.left = x;
                        drag.top = newY;
                        TweenMax.fromTo(el, 0.5, {left: el.prop('offsetLeft'), top: el.prop('offsetTop')}, {
                            left: x,
                            top: newY
                        });
                    }
                };

                /**
                 * Main drag and drop logic which is called from drag item
                 *
                 * @param {Object} dropScope Isolated scope of drop item
                 * @param  {JQLite} dragElement
                 * @param  {JQLite} dropElement
                 * @param isDoubleClick flag for indication if it is called from double click
                 */
                $scope.doDragEndDrop = function (dragScope) {
                    var dragElement = dragScope.dragScope.dragElement;
                    var dropElement = dragScope.dragScope.dropElement;
                    var dropId;
                    dragScope = dragScope.dragScope.dragScope;
                    var dropScope;

                    if (dragScope.dropId >= 0 && dropElement) {
                        dropId = parseInt(dropElement.attr("id"), 10);
                        if (dropId !== dragScope.dropId) {
                            $scope.drops[dragScope.dropId].dragId = -1;
                            $scope.drops[dragScope.dropId].isDragged = false;
                            $scope.drops[dragScope.dropId].dragScope = null;
                        }
                    }
                    if (dropElement) {
                        dropId = parseInt(dropElement.attr("id"), 10);
                        dropScope = $scope.drops[dropId];
                        dragScope.dropId = dropId;
                        dropScope.dragScope = dragScope;
                    }

                    if (dropElement) {
                        var top = parseInt(dropElement.css("top"));
                        $scope.onDragItemCall(dragScope.left, top, dragScope.top, dragScope);
                        dropScope.isDragged = true;
                        dragScope.top = top;
                        TweenMax.fromTo(dragElement, 0.5, {
                            left: dragElement.prop('offsetLeft'),
                            top: dragElement.prop('offsetTop')
                        }, {
                            left: dropElement.prop('offsetLeft') + dropElement.prop('clientWidth') - 26,
                            top: dropElement.prop('offsetTop'),
                            onComplete: function () {
                                dragElement.css('zIndex', 0);
                                $scope.hightLightDropTarget({"dropElement": "simple"});
                            }
                        });
                        dropElement.removeClass('mat-drop-box-over');
                    } else {
                        moveToOriginalPosition(dragScope, dragElement);
                    }
                    dragElement.css('zIndex', 0);
                };

                $scope.hightLightDropTarget = function (elm) {
                    var dropElement = elm.dropElement;
                    var elmLen = $scope.dropels.length;
                    for (var i = 0; i < elmLen; i++) {
                        $scope.dropels[i].removeClass('est-drop-target-over');
                    }
                    if (dropElement.elem)
                        dropElement.elem.addClass('est-drop-target-over');
                }
            },
            /**
             *
             * @param {$scope} scope
             * @param {Element} elm
             *
             */
            link: function (scope, elm, attr) {

                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                /**
                 * Property to hold correct answers.
                 * @type {boolean}
                 */
                var corrects,

                    /**
                     * Array  of properties which we can't remove.
                     * @type {string[]}
                     */
                    excludeArray = [],

                    /**
                     * middle position of the  document (browser)
                     * @type {*}
                     */
                    half,

                    /**
                     * Property to hols scoreVo
                     * @type {ScoreVo}
                     */
                    score,

                    /**
                     * Property to hold timer.
                     */
                    timer,

                    /**
                     * Array of events that we have interest.
                     * @type {Array}
                     */
                    watchers = [],

                    /**
                     * Property to hold instance of window element.
                     * @type {*|Object}
                     */
                    win = angular.element($window);


                excludeArray = ["progress", "elementWidth", "hasNextButton", "nextButtonEnabled"];

                //---------------------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------------------

                scope.itemsContainer;

                scope.itemsContainer = angular.element(elm[0].getElementsByClassName('est-items-container'));

                scope.questionElement = elm;
                //---------------------------------------------------------------------
                // Private Functions
                //---------------------------------------------------------------------

                function sortDrags(a, b) {
                    if (a.top > b.top) {
                        return 1;
                    } else if (a.top === b.top) {
                        return -1;
                    } else {
                        return 0;
                    }
                }

                function sortDragels(a, b) {
                    if (a[0].offsetTop > b[0].offsetTop) {
                        return 1;
                    } else if (a[0].offsetTop === b[0].offsetTop) {
                        return -1;
                    } else {
                        return 0;
                    }
                }

                /**
                 * Function that  check  all user  answered  items.
                 */
                function onCheckUserAnswer() {
                    var arr,
                        dragItemNum,
                        dragScope,
                        dropItemNum,
                        dropScope,
                        isCorrect,
                        len;
                    scope.drags = scope.drags.sort(sortDrags);
                    scope.dragels = scope.dragels.sort(sortDragels);
                    arr = scope.drags;
                    var userAnswer = new UserAnswerVO();
                    userAnswer.questionID = scope.establishVo.id;
                    userAnswer.position = scope.establishVo.position;
                    userAnswer.questionType = scope.establishVo.type;
                    for (i = 0; i < arr.length; i++) {
                        arr[i].dragEnable = false;
                    }

                    userAnswer.userAnswers = [];
                    userAnswer.userAnsweredPostions = [];
                    corrects = 0;
                    len = arr.length;
                    var answer;
                    for (var i = 0; i < len; i++) {
                        answer = new UserVO();
                        answer.number = scope.establishVo.position + "." + (i + 1);
                        dropScope = scope.drops[i];
                        if(!dropScope){
                            continue;
                        }
                        dragScope = dropScope.dragScope;
                        dragItemNum = dragScope ? dragScope.itemNumber : -1;
                        dropItemNum = dropScope.itemNumber;

                        isCorrect = false;
                        if (dropItemNum == dragItemNum) {
                            isCorrect = true;
                            corrects++;
                        }
                        if (dragItemNum !== -1) {
                            var originalAnswer = scope.establishVo.answers.find(function(ans){
                                    return ans.position == dragItemNum;
                                })|| {isDistractor:false};
                            userAnswer.userAnsweredPostions.push(originalAnswer.isDistractor ? 999 : dragItemNum);
                        } else {
                            if (dragScope && dragScope.dropId === -1) {
                                userAnswer.userAnsweredPostions.push(999);
                            } else {
                                userAnswer.userAnsweredPostions.push(-1);
                            }
                        }
                        answer.isCorrect = isCorrect;
                        userAnswer.userAnswers.push(answer);
                    }
                    userAnswer.userAnsweredPostions = userAnswer.userAnsweredPostions.join(',');
                    $rootScope.$emit('establish_checked', {corrects: corrects, answerVO: userAnswer});
                }
                /*function onCheckUserAnswer() {
                 var arr,
                 dragElement,
                 dragItemNum,
                 dragScope,
                 dropElement,
                 dropItemNum,
                 dropScope,
                 isCorrect,
                 len;

                 scope.drags = scope.drags.sort(sortDrags);
                 scope.dragels = scope.dragels.sort(sortDragels);
                 arr = scope.drags;
                 var userAnswer = new UserAnswerVO();
                 userAnswer.questionID = scope.establishVo.id;
                 userAnswer.position = scope.establishVo.position;
                 userAnswer.questionType = scope.establishVo.type;
                 for (i = 0; i < arr.length; i++) {
                 arr[i].dragEnable = false;
                 }

                 userAnswer.userAnswers = [];
                 userAnswer.userAnsweredPostions = [];
                 corrects = 0;
                 len = arr.length;
                 var answer;
                 for (var i = 0; i < len; i++) {
                 answer = new UserVO();
                 answer.number = scope.establishVo.position + "." + (i + 1);
                 dragElement = scope.dragels[i];
                 dragScope = arr[i];
                 dropElement = scope.dropels[dragScope.dropId];
                 dropScope = scope.drops[dragScope.dropId];
                 dragItemNum = dragScope.itemNumber;
                 dropItemNum = dropScope ? dropScope.itemNumber : -1;
                 isCorrect = false;
                 if (dropItemNum == dragItemNum) {
                 isCorrect = true;
                 corrects++;
                 }
                 if (dropItemNum !== -1) {
                 userAnswer.userAnsweredPostions.push(dragItemNum);
                 } else {
                 if (dragScope.dropId === -1) {
                 userAnswer.userAnsweredPostions.push(999);
                 } else {
                 userAnswer.userAnsweredPostions.push(-1);
                 }
                 }
                 answer.isCorrect = isCorrect;
                 userAnswer.userAnswers.push(answer);
                 }
                 userAnswer.userAnsweredPostions = userAnswer.userAnsweredPostions.join(',');
                 $rootScope.$emit('establish_checked', {corrects: corrects, answerVO: userAnswer});
                 }*/

                /**
                 * Helper method that change " and ' to their html entity.
                 * @param {string) text
                 * @returns {string) escaped string
                 */
                function escapeQuote(str) {
                    return str.replace(/"/g, "&#34;").replace(/'/g, "&#39;");
                }

                /**
                 * function for finding drag items
                 * @param letter
                 * @returns {number} Index of drag item
                 */
                function findDragItemIndex(letter) {
                    for (var i = 0; i < scope.drags.length; i++) {
                        if (scope.drags[i].label == letter) {
                            return i;
                        }
                    }
                }

                /**
                 *  Clean up method
                 */
                function onDestroy() {
                    if (timer) {
                        $timeout.cancel(timer);
                    }
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$" && excludeArray.indexOf(key) === -1) {
                            scope[key] = null;
                        }
                    }

                    win.off('resize', onResize);

                    onCheckUserAnswer = null;
                    escapeQuote = null;
                    findDragItemIndex = null;
                    onDestroy = null;
                    onResize = null;

                    watchers = null;
                    timer = null;
                    win = null;
                }


                /**
                 * Function for correcting items in
                 * dragContainer when window resizes
                 */
                function onResize() {
                    var arr,
                        dgW,
                        dpW,
                        drag,
                        elmWidth,
                        index,
                        left,
                        len,
                        newHalf,
                        temp;
                    arr = scope.itemsContainer.children();
                    elmWidth = win[0].innerWidth;
                    newHalf = Math.floor((elmWidth - EMPTY_PLACE) / 2);
                    len = arr.length;

                    for (var i = 0; i < len; i++) {
                        temp = angular.element(arr[i]);
                        temp.css('width', newHalf + 'px');
                        if (i % 2 !== 0) {
                            left = parseInt(temp.css('left'), 10);
                            index = parseInt(temp.attr('id'), 10);
                            drag = scope.drags[index];
                            dgW = half + RIGHT_DRAG;
                            dpW = half + RIGHT_DRAG_WITH_SPACE;
                            if (left === dgW) {
                                temp.css('left', (newHalf + RIGHT_DRAG) + 'px');
                            } else if (left === dpW) {
                                temp.css('left', (newHalf + RIGHT_DRAG_WITH_SPACE) + 'px');
                            }
                            drag.left = (newHalf + RIGHT_DRAG_WITH_SPACE);
                        }
                    }
                    half = newHalf;
                }


                /**
                 * function  that  draws drop and drag targets  on container.
                 */
                function positionElements() {
                    try {
                        while (scope.drags.length > 0) {
                            scope.drags.shift().$destroy();
                        }
                        while (scope.drops.length > 0) {
                            scope.drops.shift().$destroy();
                        }
                    }
                    catch (error) {
                        console.log(error);
                    }

                    var dragTag,
                        dropTag,
                        dvDrag,
                        dvDrop,
                        elmWidth,
                        gap = 10,
                        h = 100,
                        itemMatch,
                        itemRandom,
                        len,
                        y = 10;

                    scope.itemsContainer.empty();

                    scope.dragels = [];
                    scope.drags = [];
                    scope.drops = [];
                    scope.dropels = [];

                    elmWidth = win[0].innerWidth;
                    half = Math.floor((elmWidth - EMPTY_PLACE) / 2);
                    len = scope.establishVo.answers.length;

                    for (var i = 0; i < len; i++) {
                        var newScope = scope.$new();
                        itemMatch = scope.establishVo.texts[i];
                        itemRandom = scope.establishVo.answers[i];
                        if (itemMatch) {
                            newScope.dropText = $sce.trustAsHtml(itemMatch.text);
                            dropTag = "<drop-item id='" + i + "' drag-id='" + i + "' item-number='" + itemMatch.position + "' question-text='dropText'></drop-item>";
                            dvDrop = angular.element(dropTag);
                            scope.itemsContainer.append($compile(dvDrop)(newScope));
                            dvDrop.css({
                                'width': half + 'px',
                                'height': h + 'px',
                                'top': y + 'px',
                                'left': LEFT_DROPTARGET + 'px',
                                'position': 'absolute'
                            });
                        }
                        if (itemRandom) {
                            newScope.dragText = $sce.trustAsHtml(itemRandom.text);
                            dragTag = "<drag-item drag-id='-1' id='" + i + "' item-number='" + itemRandom.position + "' left='" + (half + LEFT_DRAGTARGET) + "' top='" + y + "' answer-text='dragText'></drag-item>";
                            dvDrag = angular.element(dragTag);
                            scope.itemsContainer.append($compile(dvDrag)(newScope));
                            dvDrag.css({
                                'width': half + 'px',
                                'height': h + 'px',
                                'top': y + 'px',
                                'left': (half + LEFT_DRAGTARGET) + 'px',
                                'position': 'absolute'
                            });
                            y += h + gap;
                        }
                    }
                }

                //---------------------------------------------------------------------
                // Scope (Public) Functions
                //---------------------------------------------------------------------

                //---------------------------------------------------------------------
                // Watchers (Registration Events).
                //---------------------------------------------------------------------

                win.on('resize', onResize);
                watchers.push(scope.$on("$destroy", onDestroy));
                watchers.push($rootScope.$on('check_establish', onCheckUserAnswer));

                if (scope.establishVo) {
                    positionElements();
                }
                if (scope.$parent && typeof scope.$parent.addQuestions === 'function') {
                    scope.$parent.addQuestions(scope);
                }
            }
        };

    }])
    .directive('dragItem', ["$window", "$document", "$compile", '$sce', function ($window, $document, $compile, $sce) {

        //---------------------------------------------------------------------
        // Private Variables.
        //---------------------------------------------------------------------

        var relativeToViewport = null;

        //---------------------------------------------------------------------
        // Private Functions
        //---------------------------------------------------------------------

        /**
         * Find specific element  in x and  y point of document.
         * @param {number} x
         * @param {number} y
         * @returns {AngularJS element}
         */
        function elementFromPoint(x, y) {
            var el,
                elem;

            if (!isRelativeToViewPort()) {
                x += $window.pageXOffset, y += $window.pageYOffset;
            }

            el = $document[0].elementFromPoint(x, y);
            elem = null;
            if (el) {
                elem = angular.element(el);
                elem = getDropItem(elem);
            }
            return elem;
        }

        /**
         *  Corrects point.
         * @param event
         * @returns {*|e.originalEvent|o.Event.originalEvent|originalEvent|jQuery.Event.originalEvent}
         */
        function getCoordinates(event) {
            var e,
                touches;

            touches = event.touches && event.touches.length ? event.touches : [event];
            e = (event.changedTouches && event.changedTouches[0]) ||
            (event.originalEvent && event.originalEvent.changedTouches &&
            event.originalEvent.changedTouches[0]) ||
            touches[0].originalEvent || touches[0];

            return e;
        }

        /**
         * Spans parent (max 10) until find a parent which has acu-phrase class
         * @param {HTMLDivElement}elem
         * @returns {HTMLDivElement} return if droptarget found or null if not.
         */
        function getDropItem(elem) {
            var i = 0;
            do {
                if (elem.hasClass('est-drop-target')) {
                    return elem;
                }
                elem = elem.parent();
                i++;
                if (i > 10) {
                    break;
                }
            } while (elem);
            return null;
        }

        /**
         *
         * @param element
         * @returns {number}
         */
        function getScrollY(element) {
            var yScroll = 0,
                par = element.parent();
            while (par[0]) {
                yScroll += par.prop('scrollTop') || 0;
                par = par.parent()
            }
            return yScroll;
        }

        /**
         * Utility for finding absolute x and y position relative to viewport.
         * @returns {boolean}
         */
        function isRelativeToViewPort() {
            var x,
                y;

            if (relativeToViewport !== null) {
                return relativeToViewport;
            }

            x = $window.pageXOffset ? $window.pageXOffset + $window.innerWidth - 1 : 0;
            y = $window.pageYOffset ? $window.pageYOffset + $window.innerHeight - 1 : 0;

            if (!x && !y) {
                return true;
            }
            return relativeToViewport = !$document[0].elementFromPoint(x, y);
        }

        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@?',
                left: '@',
                top: '@',
                itemNumber: '@',
                answerText: '=?'
            },
            template: '<div class="est-drag-target">\n    <div class="est-drag-target-grip"></div>\n    <div class="est-drag-target-content" ng-bind-html="answerHtml"></div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                /**
                 * Property to hold that is any work active ().
                 * @type {boolean}
                 */
                var active = false,

                    /**
                     * Property to hold distance X point on document.
                     * @type {number}
                     */
                    distX = 0,

                    /**
                     * Property to hold distance Y point on document.
                     * @type {number}
                     */
                    distY = 0,

                    /**
                     * Property to hold element height.
                     * @type {number}
                     */
                    eh = 0,

                    /**
                     * Property to hold element width.
                     * @type {number}
                     */
                    ew = 0,

                    /**
                     * Property to hold start X point of document.
                     * @type {number}
                     */
                    mouseX,

                    /**
                     * Property to hold mouse Y point of document.
                     * @type {number}
                     */
                    mouseY,

                    /**
                     *
                     * @type {number}
                     */
                    sh = 0,

                    /**
                     * Property to hold start X point of document.
                     * @type {number}
                     */
                    startX,

                    /**
                     * Property to hold start Y point of document.
                     * @type {number}
                     */
                    startY,

                    /**
                     *
                     * @type {number}
                     */
                    sw = 0,

                    /**
                     * Property to hold instance of $window.
                     * @type {JQLite}
                     */
                    win,

                    /**
                     * Property to hold x point.
                     * @type {number}
                     */
                    x = 0,

                    /**
                     * Property to hold y point.
                     * @type {number}
                     */
                    y = 0;

                win = angular.element($window);

                //---------------------------------------------------------------------
                // Scope Variables.
                //---------------------------------------------------------------------
                scope.answerHtml = scope.answerText;
                /**
                 * Property to hold that is drag enables.
                 * @type {boolean}
                 */
                scope.dragEnable = true;

                /**
                 *
                 * @type {null}
                 */
                scope.dragElement = element;

                /**
                 * Property to hold id of element.
                 * @type {Number}
                 */
                scope.id = parseInt(attrs.id, 10);

                /**
                 * Property to hold item number.
                 * @type {Number}
                 */
                scope.itemNumber = parseInt(attrs.itemNumber, 10);

                /**
                 * Property to hold ZIndex of element.
                 * @type {number}
                 */
                scope.zi = 0;

                /**
                 * Property to hold left position of element.
                 * @type {Number}
                 */
                scope.left = 0;

                /**
                 * Property to hold top position of element.
                 * @type {Number}
                 */
                scope.top = 0;

                //---------------------------------------------------------------------
                // Private Functions
                //---------------------------------------------------------------------

                /**
                 * Clean up method.
                 */
                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }
                    element.off("touchstart mousedown", onDragStart);
                    win.off("touchmove mousemove", onDrag);
                    win.off("touchend mouseup", onDragEnd);
                    win.off('touchcancel', onTouchCancel);
                    win = null;

                    onDestroy = null;
                    onDrag = null;
                    onDragEnd = null;
                    onDragStart = null;
                    onTouchCancel = null;
                }

                /**
                 * onDrag identically mousemove for touchscreen devices.
                 * @param $event
                 */
                function onDrag($event) {
                    if (!active) {
                        return;
                    }
                    if (!mouseX) {
                        return;
                    }
                    if (!scope.dragEnable) return;
                    $event.preventDefault();
                    $event = getCoordinates($event);
                    distX = $event.pageX - mouseX;
                    distY = $event.pageY - mouseY;

                    x = startX + distX;
                    x = Math.max(0, x);
                    x = x + ew > sw ? sw - ew : x;
                    y = startY + distY;
                    y = Math.max(0, y);
                    y = y + eh > sh ? sh - eh : y;
                    element.css({
                        left: x + "px",
                        top: y + "px",
                        zIndex: 1000
                    });

                    ew = element.prop('clientWidth');
                    element.css('display', 'none');
                    var elem = elementFromPoint($event.pageX, $event.pageY);
                    element.css('display', 'inline-block');
                    var obj = {elem: elem};
                    scope.$parent.hightLightDropTarget({dropElement: obj});
                }

                /**
                 * onDragEnd identically mouseUp for touchscreen devices.
                 * @param $event
                 */
                function onDragEnd($event) {
                    if (!active) {
                        return;
                    }
                    active = false;

                    if (!scope.dragEnable) {
                        return;
                    }
                    if (distX == 0 && distY === 0) {
                        return;
                    }
                    distX = 0;
                    distY = 0;
                    $event = getCoordinates($event);
                    ew = element.prop('clientWidth');
                    element.css('display', 'none');
                    var elem = elementFromPoint($event.pageX, $event.pageY);
                    element.css('display', 'inline-block');
                    var obj = {dragScope: scope, dragElement: element, dropElement: elem};
                    scope.$parent.doDragEndDrop({dragScope: obj});
                }

                /**
                 * onDragStart identically mouseDown for touchscreen devices.
                 * @param $event
                 */
                function onDragStart($event) {
                    if (!scope.dragEnable) {
                        return;
                    }

                    active = true;
                    $event = getCoordinates($event);
                    ew = element.prop('clientWidth');
                    eh = element.prop('clientHeight');
                    startX = element.prop('offsetLeft');
                    startY = element.prop('offsetTop');
                    sw = $window.innerWidth;
                    sh = $window.innerHeight;
                    mouseX = $event.pageX;
                    mouseY = $event.pageY;
                    element.css('zIndex', 1000);
                    element.css('position', 'absolute');
                }

                /**
                 * onTouchCancel identically mouseUp.
                 * @param $event
                 */
                function onTouchCancel($event) {
                    active = false;
                }


                //---------------------------------------------------------------------
                // Scope Functions
                //---------------------------------------------------------------------


                scope.$parent.$parent.addDrag(scope);

                //---------------------------------------------------------------------
                // Register events (watchers)
                //---------------------------------------------------------------------

                element.on("touchstart mousedown", onDragStart);
                win.on("touchmove mousemove", onDrag);
                win.on("touchend mouseup", onDragEnd);
                win.on('touchcancel', onTouchCancel);
                scope.$on("$destroy", onDestroy);
            }
        }
    }])
    .directive('dropItem', ["$window", "$document", "$compile", '$sce', function ($window, $document, $compile, $sce) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@?',
                position: '@',
                itemNumber: '@',
                dragId: '=?',
                questionText: '=?'
            },
            template: '<div class="est-drop-target">\n    <div class="est-drop-target-content" ng-bind-html="questionHtml"></div>\n    <div class="est-drop-target-grip"></div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                //---------------------------------------------------------------------
                // Scope Variables.
                //---------------------------------------------------------------------

                /**
                 *
                 * @type {null}
                 */
                scope.dropElement = element;

                /**
                 *
                 * @type {*}
                 */
                scope.questionHtml = scope.questionText;

                /**
                 * Property to hold id of element.
                 * @type {Number}
                 */
                scope.id = parseInt(attrs.id, 10);

                /**
                 * Property to hold id of element.
                 * @type {Number}
                 */
                scope.position = parseInt(attrs.position, 10);

                //---------------------------------------------------------------------
                // Private Functions
                //---------------------------------------------------------------------

                /**
                 * Clean up method.
                 */
                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                }

                //---------------------------------------------------------------------
                // Scope Functions
                //---------------------------------------------------------------------

                scope.$parent.$parent.addDrop(scope);
                //---------------------------------------------------------------------
                // Register events (watchers)
                //---------------------------------------------------------------------

                scope.$on("$destroy", onDestroy);
            }
        }
    }]);