angular.module('subjectsModule', [])
    .directive('subjectsView', ['$rootScope', 'appealService', function ($rootScope, appealService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isVisible: '=?',
                isFullControl: "=?"
            },
            template: '<div class="ved-container">\n    <div class="ved-table-content" ng-style="footerStyle">\n        <details style="font-family: sans-serif;" ng-repeat="exam in examTypes" ng-click="onExaminationTypeClick($event, exam)">\n            <summary ng-bind="exam.name" title="{{exam.name}}"></summary>\n            <table class="vedemosts-main-table">\n                <tbody>\n                <tr>\n                    <td style="text-align: center"><span><strong>№</strong></span></td>\n                    <td style="width: 75%;"><span><strong>Номи Фан</strong></span></td>\n                    <td style="width: 20%;text-align: center;"><span><strong>Амалиёт</strong></span></td>\n                </tr>\n                <tr ng-repeat="vedemost in exam.variants">\n                    <td class="ved-col1">{{$index+1}}</td>\n                    <td style="width: 75%;" ng-bind="vedemost.subject_name"></td>\n                    <td style="width: 20%;text-align: center;">\n                        <a href="" ng-click="onShowTestingResult(vedemost)">Натиҷаи Тест</a>\n                    </td>\n                </tr>\n                </tbody>\n            </table>\n        </details>\n    </div>\n    <div ng-show="isFullControl" class="appeal-footer" style="text-align: right;padding-right: 40px;">\n        <button style="font-size: 24px;"\n                ng-click="onReturnButtonClick()"\n                class="cw-button cw-green-medium-button">\n            <span>Бозгашт</span>\n        </button>\n    </div>\n</div>',
            link: function (scope, element, attr) {
                //-----------------------------------------------------------------------------
                // Private Variables
                //-----------------------------------------------------------------------------

                var watchers = [];
                //-----------------------------------------------------------------------------
                // Public Variables
                //-----------------------------------------------------------------------------

                scope.footerStyle = {bottom: scope.isFullControl ? '80px' : '0'};


                //-----------------------------------------------------------------------------
                // Private Functions
                //-----------------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    onDestroy = null;
                }

                function onVariantsLoaded(){
                    scope.examTypes = appealService.examinationTypes;
                    scope.footerStyle = {bottom: scope.isFullControl ? '80px' : '0'};
                    $rootScope.$$phase || scope.$apply();
                }

                function onVisibleChange(newValue, oldValue) {
                    if (newValue) {
                        onVariantsLoaded();
                    }else{
                        scope.examTypes = null;
                        $rootScope.$$phase || scope.$apply();
                    }
                }

                //-----------------------------------------------------------------------------
                // Public Functions
                //-----------------------------------------------------------------------------

                scope.onShowTestingResult = function (variant) {
                    if (variant) {
                        appealService.loadVariantQuestions(variant);
                    }
                };

                scope.onReturnButtonClick = function () {
                    $rootScope.$emit("showStudentView");
                    scope.examTypes = null;
                    $rootScope.$$phase || scope.$apply();
                };

                scope.onExaminationTypeClick = function ($event, examination) {
                    if (scope.currentExamDetails && scope.currentExamDetails !== event.currentTarget) {
                        scope.currentExamDetails.removeAttribute('open');
                    }
                    scope.currentExamDetails = event.currentTarget;
                };
                //-----------------------------------------------------------------------------
                // Watchers (Event Listeners)
                //-----------------------------------------------------------------------------

                watchers.push($rootScope.$on('variantsLoaded', onVariantsLoaded));
                watchers.push(scope.$on('$destroy', onDestroy));
                watchers.push(scope.$watch('isVisible', onVisibleChange))
            }
        }
    }]);