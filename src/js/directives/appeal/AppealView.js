angular.module('appealViewModule', ['appealQuestionTypes'])
    .directive('appealView', ['$compile', '$rootScope', 'appealService', function ($compile, $rootScope, appealService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isVisible: '=?',
                isFullControl: '=?'
            },
            template: '<div class="appeal-container">\n    <div class="appeal-header">\n        <div class="appeal-header-back" ng-click="onBackButtonClick()">Назад</div>\n        <div class="appeal-header-info">\n            <div class="appeal-header-student">{{student.FioTj}}, {{vedemost.subject_name}}</div>\n            <div class="appeal-header-score">Бал/Хол: {{score}}</div>\n        </div>\n    </div>\n    <div ng-style="footerStyle" class="appeal-questions-container"></div>\n    <div ng-show="isFullControl" class="appeal-footer">\n        <span>Бо сабаби</span>\n        <textarea cols="5" rows="2" style="width: 450px;height: 60px;resize: none;margin-top: 10px; color: #000000"\n                  ng-model="selectedComment">\n        </textarea>\n        <select ng-model="selectedAnswers">\n            <option ng-repeat="item in answersToNeed" value="{{item}}">{{item}}</option>\n        </select>\n        <span>ҷавоби дуруст илова карда шавад.</span>\n        <button style="font-size: 24px;margin-top: -40px;"\n                ng-click="onUpdateScoreButtonClick($event, selectedComment, selectedAnswers)"\n                class="cw-button cw-green-medium-button">\n            <span>Ивазкунӣ</span>\n        </button>\n    </div>\n</div>',
            link: function (scope, element, attr) {

                //-----------------------------------------------------------------------------
                // Private Variables
                //-----------------------------------------------------------------------------

                var itemsContainer,
                    examField,
                    apealField,
                    watchers = [];

                itemsContainer = angular.element(element[0].getElementsByClassName('appeal-questions-container')[0]);
                //-----------------------------------------------------------------------------
                // Public Variables
                //-----------------------------------------------------------------------------

                scope.footerStyle = {bottom: scope.isFullControl ? '80px' : '0'};


                scope.student = appealService.student;

                scope.selectedComment = "нодуруст будани саволҳо";

                scope.selectedAnswers = "";
                scope.polugodie = 1;
                scope.score = 0;
                //-----------------------------------------------------------------------------
                // Private Functions
                //-----------------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    onDestroy = null;
                }

                function onVisibleChange(newValue, oldValue) {
                    if (newValue && newValue !== oldValue) {
                        scope.footerStyle = {bottom: scope.isFullControl ? '80px' : '0'};
                        scope.userQuestions = appealService.userAnsweredQuestions;
                        scope.vedemost = appealService.currentVedemost;
                        scope.student = appealService.student;
                        scope.score = appealService.score;
                        positionQuestions();
                    }
                }


                function positionQuestions() {
                    var questionTag,
                        drvQuestion,
                        questionLen;
                    itemsContainer.empty();

                    scope.locale = appealService.locale;
                    questionLen = scope.userQuestions.length;
                    for (var i = 0; i < questionLen; i++) {
                        switch (scope.userQuestions[i].type) {
                            case QuestionTypes.SINGLE_CHOICE:
                                questionTag = '<ap-single question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.MULTIPLE_CHOICE:
                                questionTag = '<ap-multiple question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.ESTABLISH_CONFORMITY:
                                questionTag = '<ap-establish question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.INDICATION_TRUE_FALSE:
                                questionTag = '<ap-indication locale="locale" question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.MANUAL_ENTER_NUMBER:
                                questionTag = '<ap-enter-number question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.MANUAL_ENTER_TEXT:
                                questionTag = '<ap-enter-text question="userQuestions[' + i + ']">';
                                break;
                            case QuestionTypes.FILLING_THE_GAPS:
                                questionTag = '<ap-filling-gaps question="userQuestions[' + i + ']">';
                                break;
                        }
                        drvQuestion = angular.element(questionTag);
                        itemsContainer.append($compile(drvQuestion)(scope));
                    }
                }

                function createItems(answers) {
                    var arr = [];
                    for (var i = 0; i < answers; i++) {
                        arr.push(i + 1);
                    }
                    return arr;
                }

                function onVedemostResultLoaded() {
                    scope.vedemostResult = appealService.vedemostResult;
                    var currentVedemost = appealService.currentVedemost;
                    examField = 'vd_Rex';
                    apealField = 'vd_RexApel';

                    switch (currentVedemost.exam_type) {
                        case "1":
                            examField = "vd_R1_Test";
                            apealField = "vd_R1Apel";
                            break;
                        case "2":
                            examField = "vd_R2_Test";
                            apealField = "vd_R2Apel";
                            break;
                        case "3":
                            examField = "vd_RexTeacher";
                            apealField = "vd_RexTeacherApel";
                            break;
                        case "4":
                            examField = "vd_Rex";
                            apealField = "vd_RexApel";
                            break;
                        case "5":
                            examField = "vd_Tex";
                            apealField = "vd_TexApel";
                            break;
                    }
                    scope.score = +(scope.vedemostResult[examField]) + +(scope.vedemostResult[apealField]);
                    var totalCorrects = appealService.totalCorrects;
                    var factor = +(appealService.currentVedemost.factor);
                    scope.balPerQuestion = Math.round(((100 * factor) / appealService.totalCorrects) * 100) / 100;
                    scope.userAnswered = Math.round(scope.score / scope.balPerQuestion);
                    scope.answersToNeed = totalCorrects - scope.userAnswered;
                    scope.answersToNeed = createItems(scope.answersToNeed);
                }


                function onPolugodieChange(event, polugodie) {
                    scope.polugodie = polugodie;
                }

                //-----------------------------------------------------------------------------
                // Public Functions
                //-----------------------------------------------------------------------------

                scope.onBackButtonClick = function () {
                    if (window.onCloseApealClicked) {
                        window.onCloseApealClicked();
                    } else {
                        scope.selectedComment = "нодуруст будани саволҳо";
                        scope.selectedAnswers = "";
                        appealService.getStudentVariants(appealService.studentId.replace("/", ""), scope.polugodie);
                    }
                };

                scope.onUpdateScoreButtonClick = function ($event, comment, answers) {
                    if (comment.length === 0 || answers.length === 0) {
                        return false;
                    }
                    var fullComment = "Бо сабаби " + comment + ", " + answers + " ҷавоби дуруст илова карда шавад.";
                    var bal = scope.balPerQuestion * answers;
                    var prevBal = scope.score;
                    scope.score += bal;
                    scope.score = Math.round(scope.score * 100) / 100;
                    scope.score = scope.score > 100 ? 100 : scope.score;
                    var appealBal = scope.score - prevBal;
                    appealService.updateUserAppealScore(fullComment, appealBal);
                };
                //-----------------------------------------------------------------------------
                // Watchers (Event Listeners)
                //-----------------------------------------------------------------------------

                watchers.push(scope.$on('$destroy', onDestroy));

                watchers.push(scope.$watch('isVisible', onVisibleChange));

                watchers.push($rootScope.$on('appealResultUpdated', scope.onBackButtonClick));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push($rootScope.$on('vedemostResultLoaded', onVedemostResultLoaded))

            }
        }
    }]);