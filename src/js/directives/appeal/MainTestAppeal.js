angular.module('mainTestAppeal', ['subjectsModule', 'appealViewModule','appealService'])
    .directive('mainTestAppeal', ['appealService', '$rootScope', function (appealService, $rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                isFullControl: '=?',
                user: '=?',
                userName: '=?',
                studentId: '=?'
            },
            template: '<div class="tst-appeal-main">\n    <subjects-view ng-show="isSubjectsView" is-visible="isSubjectsView" is-full-control="isFullControl"></subjects-view>\n    <appeal-view ng-show="isAppealView" is-visible="isAppealView" is-full-control="isFullControl"></appeal-view>\n</div>',
            link: function (scope, element, attr) {

                //-----------------------------------------------------------------------------
                // Private Variables
                //-----------------------------------------------------------------------------

                var watchers = [];

                appealService.httpOptions.headers = appealService.httpOptions.headers || {};
                appealService.httpOptions.headers.user = scope.user;
                appealService.studentId = scope.studentId;

                //-----------------------------------------------------------------------------
                // Public Variables
                //-----------------------------------------------------------------------------

                scope.isSubjectsView = false;

                scope.isAppealView = false;
                scope.polugodie = 1;
                //-----------------------------------------------------------------------------
                // Private Functions
                //-----------------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }
                    onDestroy = null;
                }

                function onVariantsLoaded() {
                    scope.isAppealView = false;
                    scope.isSubjectsView = true;
                }

                function onTestResultLoaded() {
                    scope.isAppealView = true;
                    scope.isSubjectsView = false;
                }

                function getData() {
                    if (scope.user && scope.studentId) {
                        appealService.getStudentVariants(appealService.studentId.replace("/", ""), scope.polugodie);
                    }
                }

                function onUserIdChange(newValue) {
                    if (newValue) {
                        appealService.httpOptions.headers.user = scope.user;
                        getData();
                    }
                }
                function onUserNameChange(newValue) {
                    if (newValue) {
                        appealService.userName = newValue;
                    }
                }

                function onStudentIdChange(newValue) {
                    if (newValue) {
                        appealService.studentId = scope.studentId;
                        appealService.initConfig([0, scope.studentId]);
                        getData();
                    }
                }

                function onPolugodieChange(event, polugodie){
                    scope.polugodie = polugodie;
                    getData();
                }
                //-----------------------------------------------------------------------------
                // Public Functions
                //-----------------------------------------------------------------------------

                //-----------------------------------------------------------------------------
                // Watchers (Event Listeners)
                //-----------------------------------------------------------------------------

                watchers.push(scope.$on('$destroy', onDestroy));
                watchers.push($rootScope.$on('variantsLoaded', onVariantsLoaded));
                watchers.push($rootScope.$on('testResultLoaded', onTestResultLoaded));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push(scope.$watch('user', onUserIdChange));
                watchers.push(scope.$watch('userName', onUserNameChange));
                watchers.push(scope.$watch('studentId', onStudentIdChange));
                getData();
            }
        }
    }]);