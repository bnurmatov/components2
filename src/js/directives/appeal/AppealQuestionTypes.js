angular.module('appealQuestionTypes', [])
    .directive('apSingle', [function () {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder" ng-disabled="true">\n    <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.answers">\n            <span class="as-answer-index" ng-bind="item.index"></span>\n            <input class="as-answer-checkbox" type="checkbox" ng-model="item.isCorrect">\n\n            <div class="as-answer-text" ng-class="{\'correct\':item.isCorrect}" ng-bind-html="item.text"></div>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.userAnswers">\n            <span class="as-answer-index" ng-bind="item.index"></span>\n            <input class="as-answer-checkbox" type="checkbox" ng-model="item.selected">\n\n            <div class="as-answer-text"\n                 ng-class="{\'correct\': item.selected && item.isCorrect, \'incorrect\': item.selected && !item.isCorrect }"\n                 ng-bind-html="item.text"></div>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------

                var watchers = [];

                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------
            }
        }
    }])
    .directive('apMultiple', [function () {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder">\n    <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.answers">\n            <span class="as-answer-index" ng-bind="item.index"></span>\n            <input class="as-answer-checkbox" type="checkbox" ng-model="item.isCorrect">\n            <div class="as-answer-text" ng-class="{\'correct\':item.isCorrect}" ng-bind-html="item.text"></div>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.userAnswers">\n            <span class="as-answer-index" ng-bind="item.index"></span>\n            <input class="as-answer-checkbox" type="checkbox" ng-model="item.selected">\n            <div class="as-answer-text"\n                 ng-class="{\'correct\': item.selected && item.isCorrect, \'incorrect\': item.selected && !item.isCorrect }"\n                 ng-bind-html="item.text"></div>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------
            }
        }
    }])
    .directive('apIndication', [function () {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?',
                locale: '=?'
            },
            template: '<div class="appeal-question-holder">\n    <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.answers">\n            <span class="as-answer-index">{{item.index}}.</span>\n\n            <div class="ai-answer-text" ng-bind-html="item.text"></div>\n            <select  disabled>\n                <option selected value="{{item.isCorrect? indications[0].value : indications[1].value}}">\n                    {{item.isCorrect ? indications[0].title : indications[1].title}}\n                </option>\n            </select>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.userAnswers">\n            <span class="as-answer-index">{{item.index}}.</span>\n\n            <div class="ai-answer-text" ng-bind-html="item.text"></div>\n            <select ng-class="{\'ai-correct\':(item.selected === 1 && item.isCorrect || item.selected === 0 && !item.isCorrect ), \'ai-incorrect\':(item.selected === 0 && item.isCorrect || item.selected === 1 && !item.isCorrect )}"\n                    disabled>\n                <option selected value="{{item.selected === 1 ? indications[0].title : indications[1].title}}">\n                    {{item.selected === 1 ? indications[0].title : indications[1].title}}\n                </option>\n            </select>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------

                function getIndicationsByLanguage() {
                    var indic = [{title: 'Дуруст', value: true}, {title: 'Нодуруст', value: false}];
                    if (scope.locale) {
                        if (scope.locale === "ru_RU") {
                            return [{title: 'Правильно', value: true}, {
                                title: 'Неправильно',
                                value: false
                            }];
                        }
                        else if (scope.locale === "en_US") {
                            return [{title: 'Correct', value: true}, {title: 'Incorrect', value: false}];
                        }
                    }
                    return indic;
                }


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------

                scope.indications = getIndicationsByLanguage();
            }
        }
    }])
    .directive('apEstablish', ['$sce', function ($sce) {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder">\n    <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.answers">\n            <span class="as-answer-index">{{item.index}}.</span>\n\n            <div class="aest-answer-holder">\n                <div class="aest-text" ng-class="{\'aest-correct\':item.text}" ng-bind-html="item.text"></div>\n                <div class="aest-answer aest-correct" ng-bind-html="item.answer"></div>\n            </div>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer" ng-repeat="item in question.userAnswers">\n            <span class="as-answer-index">{{$index + 1}}.</span>\n\n            <div class="aest-answer-holder">\n                <div class="aest-text"\n                     ng-class="{\'aest-text-border\':item.text, \'aest-correct\': item.text && item.isCorrect, \'aest-incorrect\': item.text && !item.isCorrect && item.isAnswered}"\n                     ng-bind-html="item.text"></div>\n                <div class="aest-answer"\n                     ng-class="{\'aest-correct\':item.isCorrect, \'aest-incorrect\':!item.isCorrect && item.isAnswered}"\n                     ng-bind-html="item.answer"></div>\n            </div>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------

                if (scope.question) {
                    for (var i = 0; i < scope.question.answers.length; i++) {
                        scope.question.answers[i].text = $sce.trustAsHtml(scope.question.answers[i].text);
                        scope.question.answers[i].answer = $sce.trustAsHtml(scope.question.answers[i].answer);
                        scope.question.userAnswers[i].isAnswered = scope.question.userAnswers[i].answer.length > 0;
                        scope.question.userAnswers[i].text = $sce.trustAsHtml(scope.question.userAnswers[i].text);
                        scope.question.userAnswers[i].answer = $sce.trustAsHtml(scope.question.userAnswers[i].isAnswered ? scope.question.userAnswers[i].answer : '<span></span>');
                    }
                }
            }
        }
    }])
    .directive('apEnterText', [function () {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder">\n    <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer">\n            <span class="as-answer-index">1. </span>\n\n            <div class="as-answer-text" ng-bind="question.answers[0]"></div>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer">\n            <span class="as-answer-index">1. </span>\n\n            <div class="ap-ent-text-answer"\n                 ng-class="{\'ap-ent-text-answer-incorrect\':!question.userAnswers[0].isCorrect, \'ap-ent-text-answer-correct\':question.userAnswers[0].isCorrect}"\n                 ng-bind="question.userAnswers[0].text">\n            </div>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------
            }
        }
    }])
    .directive('apEnterNumber', [function () {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder">\n   <div class="appeal-question-title">\n        <span>{{question.position}}. </span><span ng-bind-html="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="as-answer">\n            <span class="as-answer-index">1. </span>\n\n            <div class="as-answer-text" ng-bind="question.answers[0]"></div>\n        </div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="as-answer">\n            <span class="as-answer-index">1. </span>\n\n            <div class="ap-ent-text-answer"\n                 ng-class="{\'ap-ent-text-answer-incorrect\':!question.userAnswers[0].isCorrect, \'ap-ent-text-answer-correct\':question.userAnswers[0].isCorrect}"\n                 ng-bind="question.userAnswers[0].text">\n            </div>\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------
            }
        }
    }])
    .directive('apFillingGaps', ['$sce', function ($sce) {
        return {
            restrict: 'E',
            replace: 'true',
            scope: {
                question: '=?'
            },
            template: '<div class="appeal-question-holder">\n    <div class="appeal-question-title">\n        <span>{{question.position}}.  </span><span ng-bind="question.title"></span>\n    </div>\n    <div class="appeal-answers-holder">\n        <div class="af-answer" ng-bind-html="question.answers[0]"></div>\n    </div>\n    <div class="appeal-user-answers-holder">\n        <div class="af-answer" ng-bind-html="question.userAnswers[0]"></div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Variables
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Private Function
                //---------------------------------------------------------------------


                //---------------------------------------------------------------------
                // Public Function
                //---------------------------------------------------------------------

                if (scope.question) {
                    scope.question.answers[0] = $sce.trustAsHtml(scope.question.answers[0]);
                    scope.question.userAnswers[0] = $sce.trustAsHtml(scope.question.userAnswers[0]);
                }

                //---------------------------------------------------------------------
                // Watchers
                //---------------------------------------------------------------------
            }
        }
    }]);