angular.module('scaleToFit', [])
    .directive('scaleToFit', ['$window', function ($window) {
        return{
            restrict: 'A',
            link: function (scope, element) {
                //---------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------

                var unDestroy,
                    rootDiv,
                    minWidth = 1024,
                    minHeight = 768,
                    win = angular.element($window);

                rootDiv = element[0];
                rootDiv.style.MozTransformOrigin = "0 0";
                rootDiv.style.msTransformOrigin = "0 0";
                rootDiv.style.transformOrigin = "0 0";
                rootDiv.style.webkitTransformOrigin = "0 0";
                //---------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------

                //---------------------------------------------------------
                // Private Methods
                //---------------------------------------------------------

                function onResize() {

                    var ratio = Math.min(1, Math.min($window.innerWidth / minWidth, $window.innerHeight / minHeight));
                    if (ratio === 1) {
                        rootDiv.style.height = "100%";
                        rootDiv.style.width = "100%";
                    } else {
                        rootDiv.style.height = Math.round($window.innerHeight / ratio) + "px";
                        rootDiv.style.width = Math.round($window.innerWidth / ratio) + "px";
                    }
                    var transfromString = "scale(" + ratio + ")";

                    rootDiv.style.MozTransform = transfromString;
                    rootDiv.style.msTransform = transfromString;
                    rootDiv.style.OTransform = transfromString;
                    rootDiv.style.transform = transfromString;
                    rootDiv.style.webkitTransform = transfromString;

                    $window.scaleToFitFactor = ratio;
                    win.triggerHandler('orientationchange');
                }

                /**
                 *
                 * @param event
                 */
                function onDestroy() {
                    win.off('resize', onResize);
                    unDestroy();
                    onDestroy = null;
                    onResize = null;

                    unDestroy = null;
                    win = null;
                }

                //---------------------------------------------------------
                // Public Methods
                //---------------------------------------------------------
                /**
                 *  Listen to stage keydwon event
                 */
                win.on('resize', onResize);
                unDestroy = scope.$on('$destroy', onDestroy);
                onResize();
            }
        };
    }]);