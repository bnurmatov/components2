/**
 * Created by Komron Musoev on 9/18/14.
 */

angular.module('multipleQuestion', [])
    .directive('multipleQuestion', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                multipleVo: '=?',
                number: '@?',
                addQuestions: '&?'
            },
            template: '<div class="tst-multiple-question">\n    <div style="display: table;">\n        <span style="display: table-cell;" class="tst-multiple-question-text">{{number}}.</span>\n        <span style="display: table-cell;" ng-bind-html="multipleVo.text"></span>\n        <span>(Шумораи ҷавобҳои дуруст {{totalForCheck}} )</span>\n    </div>\n\n    <div style="display: table;" class="tst-multiple-answers-holder" ng-repeat="answer in multipleVo.answers">\n        <span style="display: table-cell;">{{$index + 1}}.</span>\n        <input style="display: table-cell;" type="checkbox" ng-click="onCheckBoxClick($event, $index)">\n        <span style="display: table-cell;" ng-bind-html="answer.text"></span>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------
                var watchers = [],
                    checkedItems = [];

                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                scope.totalForCheck = 0;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------

                function getTotalForCheck(answers) {
                    for (var i = 0; i < answers.length; i++) {
                        if (Base64.decode(answers[i].hashSumma).split("#")[0] == 1) {
                            scope.totalForCheck++;
                        }
                    }
                    $rootScope.$$phase || scope.$apply();
                }

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;

                    watchers = null;
                }

                function disableCheckBoxs() {
                    var checkboxs = element.find('input');
                    for (var i = 0; i < checkboxs.length; i++) {
                        if (!checkboxs[i].checked) {
                            angular.element(checkboxs[i]).attr('disabled', true);
                        }
                    }
                }

                function enableCheckBoxs() {
                    var checkboxs = element.find('input');
                    for (var i = 0; i < checkboxs.length; i++) {
                        angular.element(checkboxs[i]).attr('disabled', false);
                    }
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.onCheckBoxClick = function ($event, id) {
                    var target = $event.currentTarget;
                    if (target.checked) {
                        checkedItems.push(id);
                        if (checkedItems.length == scope.totalForCheck) {
                            disableCheckBoxs();
                        }
                    } else {
                        var index = checkedItems.indexOf(id);
                        checkedItems.splice(index, 1);
                        enableCheckBoxs();
                    }
                };

                scope.addQuestions({questionScope: scope});
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------
                watchers.push($rootScope.$on('$destroy', onDestroy));

                getTotalForCheck(scope.multipleVo.answers);
            }
        }
    }]);