/**
 * Created by Komron Musoev on 9/18/14.
 */
angular.module('enterText', [])
    .directive('enterText', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                enterTextVo: '=?',
                number: '@?',
                addQuestions: '&?',
                showAnswer: '@?'
            },
            template: '<div class="tst-enter-text-question">\n    <div style="display: table;">\n        <span style="display: table-cell;" class="tst-enter-text-question-text">{{number}}.</span>\n        <span style="display: table-cell;" ng-bind-html="enterTextVo.text"></span>\n    </div>\n    <div class="tst-enter-text-answer-holder">\n        <input type="text" ng-model="enterTextVo.answer.text" ng-show="{{showAnswer == 1}}">\n        <div style="display: inline-block;width: 280px;height: 80px;">\n            <div ng-show="{{showAnswer != 1}}" style="width: 100%;height: 32px;padding-left: 20px;">\n                <button ng-click="onKButtonClick()">қ</button>\n                <button ng-click="onXButtonClick()">ҳ</button>\n                <button ng-click="onJButtonClick()">ҷ</button>\n                <button ng-click="onGButtonClick()">ғ</button>\n                <button ng-click="onUButtonClick()">ӯ</button>\n                <button ng-click="onIButtonClick()">ӣ</button>\n            </div>\n            <input class="simple-input" style="width: 100%;font-size: 24px;" type="text" ng-paste="onInputPasteEvent($event)" ng-keydown="onInputKeyDown($event)" ng-keypress="onInputKeyPress($event)" ng-show="{{showAnswer != 1}}">\n        </div>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                var watchers = [],
                    input;

                input = element[0].getElementsByClassName('simple-input')[0];
                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;
                    watchers = null;
                }

                function setCharAt(str, index, chr) {
                    if (index > str.length - 1) return str + chr;
                    return str.substr(0, index) + chr + str.substr(index);
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.addQuestions({questionScope: scope});

                scope.onInputKeyPress = function ($event) {
                    if ($event.charCode === 36 || $event.charCode === 42) {
                        $event.preventDefault();
                    }
                };

                scope.onInputKeyDown = function ($event) {
                    if (($event.ctrlKey && $event.charCode === 86) || $event.which === 3) {
                        $event.preventDefault();
                    }
                };

                scope.onInputPasteEvent = function ($event) {
                    $event.preventDefault();
                };


                scope.onKButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('049B', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.setSelectionRange(prevSelection + 1, prevSelection + 1);
                    input.focus();
                };
                scope.onXButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('04B3', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.setSelectionRange(prevSelection + 1, prevSelection + 1);
                    input.focus();
                };
                scope.onJButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('04B7', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.setSelectionRange(prevSelection + 1, prevSelection + 1);
                    input.focus();
                };
                scope.onGButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('0493', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.setSelectionRange(prevSelection + 1, prevSelection + 1);
                    input.focus();
                };
                scope.onUButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('04EF', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.focus();
                };
                scope.onIButtonClick = function () {
                    var prevSelection = input.selectionStart;
                    var symbol = String.fromCharCode(parseInt('04E3', 16));
                    input.value = setCharAt(input.value, prevSelection, symbol);
                    input.setSelectionRange(prevSelection + 1, prevSelection + 1);
                    input.focus();
                };
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);
