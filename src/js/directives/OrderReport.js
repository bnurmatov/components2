angular.module('orderReport', ['tService'])
    .directive('orderReport', ['$rootScope', '$timeout', 'tService',
        function ($rootScope, $timeout, tService) {
            return {
                restrict: 'E',
                replace: true,
                scope:{
                    order:'=?'
                },
                //language=HTML
                template: '<div class="order-report-holder">\n    <div class="commons">\n        <button class="cw-button cw-green-medium-button"\n                ng-click="printClicked()">\n            Чопкуни\n        </button>\n        <button class="cw-button cw-green-medium-button"\n                ng-click="onBackClicked()">\n            Бозгашт\n        </button>\n    </div>\n    <div class="order-number" ng-bind="props.order.id"></div>\n    <div class="order-header">\n        Ба ректори Донишгоҳи Техникии Тоҷикистон ба номи академик М.С.Осимӣ Одиназода Ҳ.О. <br>аз номи донишҷуи курси {{props.studentInfo.Course}} ихтисоси <br>{{props.studentInfo.KodGroup}} {{props.studentInfo.fFac_NameTaj}} <br> {{props.studentInfo.FioTj}}\n    </div>\n    <div class="order-title">Ариза</div>\n    <div class="order-text">Дар баробари аризаи худ аз Шумо эҳтиромона хоҳиш менамоям, ки барои иштирок намудан дар триместр (сесияи иловагӣ) иҷозат диҳед. Ман аз нимсолаи {{order.polyear}} соли таҳсили {{order.uchebYearStr}} аз фанҳои зерин қарзи академӣ дорам.</div>\n    <div>\n        <table class="order-report-table">\n            <tr>\n                <th class="rt-td">р/т</th>\n                <th class="subject-td">Номи фан</th>\n                <th class="credits-td">Миқдори кредитҳо</th>\n                <th class="credit-price">Маблағи кредитҳо</th>\n                <th class="sign-td">Имзои бақайдгиранда</th>\n            </tr>\n            <tr ng-repeat="ved in props.order.vedemosts">\n                <td  class="rt-td">{{$index + 1}}</td>\n                <td class="subject-td" ng-bind="ved.subject"></td>\n                <td class="credits-td" ng-bind="ved.totalCredits"></td>\n                <td class="credit-price" ng-show="hasLessThan45Mark">{{ved.totalMark < 45 ? ((ved.totalCredits * props.order.price) != (ved.totalCredits * props.order.price).toFixed(2)) ?(Math.ceil((ved.totalCredits * props.order.price) * 10) / 10).toFixed(2) : (ved.totalCredits * props.order.price) : 0}}</td>\n                <td class="credit-price" ng-show="!hasLessThan45Mark">{{((ved.totalCredits * props.order.price) != (ved.totalCredits * props.order.price).toFixed(2)) ?(Math.ceil((ved.totalCredits * props.order.price) * 10) / 10).toFixed(2) : (ved.totalCredits * props.order.price)}}</td>\n                <td class="sign-td"></td>\n            </tr>\n            <tr>\n                <td class="rt-td" ></td>\n                <td class="subject-td td-border-none"><strong>Ҳамагӣ:</strong></td>\n                <td class="credits-td td-border-none"></td>\n                <td class="credit-price" ng-bind="props.order.summa"></td>\n                <td class="sign-td td-border-none"></td>\n            </tr>\n        </table>\n    </div>\n    <div class="order-agrement">Маблағи иловагиро барои азхудкунии фанҳои мазкур вобаста ба кредитҳо пардохт менамоям.</div>\n    <div class="empty-line"></div>\n    <div class="sign-text">Имзо:________________</div>\n    <div class="sign-text">Сана: <span ng-bind="props.order.creationTime"></span></div>\n</div>',
                link: function (scope, element) {
                    //------------------------------------------------------------------------
                    // Private variables
                    //------------------------------------------------------------------------
                    var watchers = [],
                        timerId,
                        controlsDiv = element.find('div')[0];
                    //------------------------------------------------------------------------
                    // Public variables
                    //------------------------------------------------------------------------
                    scope.props = tService.localeProps;
                    scope.hasLessThan45Mark = tService.config.uchebYear >= 16;
                    scope.Math = window.Math;
                    //---------------------------------------------------------
                    // Private Methods
                    //---------------------------------------------------------

                    /**
                     * Remove event listeners and clean data on destroy
                     */
                    function onDestroy() {
                        while (watchers.length > 0) {
                            watchers.shift()();
                        }
                        for (var key in scope) {
                            if (key[0] !== "$") {
                                scope[key] = null;
                            }
                        }

                        onDestroy = null;

                        watchers = null;
                    }

                    function onOrderChange(order){
                        if(order){
                            tService.getStudentInfo(order.student_id);
                        }
                    }
                    function showControls(){
                        $timeout.cancel(timerId);
                        if(controlsDiv){
                            controlsDiv.style.display = 'block';
                        }
                    }
                    //---------------------------------------------------------
                    // Public Methods
                    //---------------------------------------------------------
                    /**
                     * Create new order
                     */
                    scope.printClicked = function(){
                        controlsDiv.style.display = 'none';
                        PrintUtil.printHtmlElement(element[0]);
                        timerId = $timeout(showControls,1000);
                    };

                    scope.onBackClicked = function(){
                        $rootScope.$emit('changeView', 'createOrderView');
                    };

                    /**
                     * Add event listeners block
                     */
                    watchers.push(scope.$watch('order', onOrderChange));
                    watchers.push($rootScope.$on('$destroy', onDestroy));

                }
            }
        }]);