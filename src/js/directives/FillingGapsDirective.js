/**
 * Created by Komron Musoev on 9/18/14.
 */

angular.module('fillingGaps', [])
    .directive('fillingGaps', ['$rootScope', '$compile', function ($rootScope, $compile) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                fillingVo: '=?',
                number: '@?',
                addQuestions: '&?'
            },
            template: '<div class="tst-filling-question">\n    <span class="tst-filling-question-text">{{number}}. {{fillingVo.title}}</span>\n\n    <div class="tst-filling-quesion-holder"></div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------
                var watchers = [];

                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;
                    watchers = null;
                }

                function onFillingVOChanged(newValue, oldValue) {
                    if (newValue) {
                        var contentHolder = angular.element(element[0].getElementsByClassName('tst-filling-quesion-holder'));
                        contentHolder.empty();
                        var content = $compile(scope.fillingVo.text)(scope);
                        contentHolder.append(content);
                    }
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.addQuestions({questionScope: scope});
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------
                watchers.push($rootScope.$on('$destroy', onDestroy));
                watchers.push(scope.$watch('fillingVo', onFillingVOChanged));
            }
        }
    }]);