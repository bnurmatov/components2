angular.module('loadingBar', [])
    .directive('loadingBar', ['$rootScope', 'locale', '$timeout', function ($rootScope, locale, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            //language=HTML
            template: '<div class="loading-bar-popup" ng-show="isVisible" ng-click="isVisible=false">\n    <div class="loading-bar-ajax_notification" ng-show="isProgress">\n        <div class="loader"></div>\n        <div class="loading-bar-progress-text" ng-bind="locale.loadingLabel"></div>\n    </div>\n    <span class="loading-bar-ajax_notification"\n          ng-show="isError">\n        <div class="loading-bar-error">\n            <span class="glyphicon glyphicon-exclamation-sign" style="color: rgb(174, 15, 15);font-size: 16px;"></span>\n            <span ng-bind="locale.loadingErrorLabel" style="padding-left: 5px;"></span>\n        </div>\n    </span>\n    <span class="loading-bar-ajax_notification"\n          ng-show="isSuccess">\n        <div class="loading-bar-success">\n            <span class="glyphicon glyphicon-ok" style="color: green;font-size: 16px;"></span>\n            <span ng-bind="locale.loadingSuccessLabel" style="padding-left: 5px;"></span>\n        </div>\n    </span>\n</div>',
            link: function (scope) {
                //------------------------------------------------------------------------
                // Private variables
                //------------------------------------------------------------------------
                var timerId,
                    watchers = [];
                //------------------------------------------------------------------------
                // Public variables
                //------------------------------------------------------------------------
                scope.managers = null;
                scope.isVisible = false;
                scope.isProgress = false;
                scope.isSuccess = false;
                scope.isError = false;
                //---------------------------------------------------------
                // Private Methods
                //---------------------------------------------------------

                function resetStates(){
                    scope.isProgress = false;
                    scope.isSuccess = false;
                    scope.isError = false;
                }

                function onIsVisibleChange(newValue, oldValue){
                    if(newValue){
                    } else {
                    }
                    $rootScope.$$phase || scope.$apply();
                }

                function onLocaleLoaded(){
                    scope.locale = locale.props;
                    if(!scope.locale.loadingLabel){
                        scope.locale = {loadingLabel:"Боркуни...",
                            loadingErrorLabel:"Хато: Дархост иҷро нашуд.",
                            loadingSuccessLabel:"Дархост иҷро шуд."};
                    }
                    $rootScope.$$phase || scope.$apply();
                }
                /**
                 * Remove event listeners and clean data on destroy
                 */
                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;
                    onLocaleLoaded = null;

                    watchers = null;
                }

                function hidePopup(){
                    $timeout.cancel(timerId);
                    resetStates();
                    scope.isVisible = false;
                    $rootScope.$$phase || scope.$apply();
                }

                function onSpinHandler(event){
                    resetStates();
                    scope.isVisible = true;
                    if(event.name == "spinStart"){
                        scope.isProgress = true;
                    } else if(event.name == "spinEnd"){
                        scope.isSuccess = true;
                        timerId = $timeout(hidePopup, 1000);
                    } else if(event.name == "spinError"){
                        scope.isError = true;
                    }
                    $rootScope.$$phase || scope.$apply();
                }
                //---------------------------------------------------------
                // Public Methods
                //---------------------------------------------------------
                /**
                 * Add event listeners block
                 */
                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push($rootScope.$on('$destroy', onDestroy));
                watchers.push($rootScope.$on('localeLoaded', onLocaleLoaded));
                watchers.push($rootScope.$on('spinStart', onSpinHandler));
                watchers.push($rootScope.$on('spinEnd', onSpinHandler));
                watchers.push($rootScope.$on('spinError', onSpinHandler));
                onLocaleLoaded();
            }
        }
    }]);