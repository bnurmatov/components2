/**
 * Created by Komron Musoev on 9/18/14.
 */
angular.module('indicationTrueFalse', [])
    .directive('indicationTrueFalse', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                indicationVo: '=?',
                number: '@?',
                addQuestions: '&?',
                config: '=?'
            },
            template: '<div class="tst-indication-question">\n    <div style="display: table;">\n        <span style="display: table-cell;" class="tst-indication-question-text">{{number}}.</span>\n        <span style="display: table-cell;" ng-bind-html="indicationVo.text"></span>\n    </div>\n    <div style="display: table;" class="tst-indication-answers-holder" ng-repeat="answer in indicationVo.answers">\n        <span style="display: table-cell;">{{$index + 1}}.</span>\n        <span style="display: table-cell;" ng-bind-html="answer.text"></span>\n        <select style="display: table-cell;">\n            <option ng-show="false" selected disabled value=""></option>\n            <option ng-repeat="indication in indications" value="{{indication.value}}">{{indication.title}}</option>\n        </select>\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                var watchers = [];
                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------
                function getIndicationsByLanguage(){
                    var indic = [{title: 'Дуруст',value: 'Дуруст'},{title: 'Нодуруст',value: 'Нодуруст'}];
                    if(scope.config){
                        if(scope.config.locale === "ru_RU"){
                            return [{title: 'Правильно',value: 'Правильно'},{title: 'Неправильно',value: 'Неправильно'}];
                        }
                        else if(scope.config.locale === "en_US"){
                            return [{title: 'Correct',value: 'Correct'},{title: 'Incorrect',value: 'Incorrect'}];
                        }
                    }
                    return indic;
                }

                function onDestroy() {

                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    onDestroy = null;

                    watchers = null;
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.addQuestions({questionScope: scope});
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------
                watchers.push($rootScope.$on('$destroy', onDestroy));
                scope.indications = getIndicationsByLanguage();
            }
        }
    }]);
