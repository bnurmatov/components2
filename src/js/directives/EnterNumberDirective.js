/**
 * Created by Musoev Komron on 9/18/14.
 */

angular.module('enterNumber', [])
    .directive('enterNumber', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                enterNumberVo: '=?',
                number: '@?',
                addQuestions: '&?'
            },
            template: '<div class="tst-enter-number-question">\n    <div style="display: table;">\n        <span style="display: table-cell;" class="tst-enter-text-question-text">{{number}}.</span>\n        <span style="display: table-cell;" ng-bind-html="enterNumberVo.text"></span>\n    </div>\n    <div class="tst-enter-number-answer-holder">\n        <input type="text"\n               ng-model="inputModel"\n               ng-paste="onInputPasteEvent($event)"\n               ng-keydown="onInputKeyDown($event)"\n               ng-keypress="onInputKeyPress($event)">\n    </div>\n</div>',
            link: function (scope, element, attrs) {
                //---------------------------------------------------------------------
                // Private Variables.
                //---------------------------------------------------------------------

                var watchers = [];

                //---------------------------------------------------------------------
                // Public Variables.
                //---------------------------------------------------------------------

                scope.questionElement = element;

                //---------------------------------------------------------------------
                // Private Functions.
                //---------------------------------------------------------------------

                function onDestroy() {
                    while (watchers.length > 0) {
                        watchers.shift()();
                    }

                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }
                    onDestroy = null;
                    watchers = null;
                }

                //---------------------------------------------------------------------
                // Public Functions.
                //---------------------------------------------------------------------

                scope.inputModel = '';

                scope.addQuestions({questionScope: scope});

                scope.onInputKeyPress = function ($event) {
                    if ($event.charCode === 45 || $event.charCode === 46 || ($event.charCode > 47 && $event.charCode <= 57)) {
                        if ($event.keyCode === 45 && scope.inputModel.length !== 0) {
                            $event.preventDefault();
                        }
                        if (($event.charCode == 46 && scope.inputModel.indexOf('.') !== -1) ||
                            ($event.charCode == 46 && scope.inputModel[0] === '-' && scope.inputModel.length === 1) ||
                            ($event.charCode == 46 && scope.inputModel.length === 0)) {
                            $event.preventDefault();
                        }
                    } else if ($event.keyCode !== 8) {
                        $event.preventDefault();
                    }
                };

                scope.onInputKeyDown = function ($event) {
                    if (($event.ctrlKey && $event.charCode === 86) || $event.which === 3) {
                        $event.preventDefault();
                    }
                };

                scope.onInputPasteEvent = function ($event) {
                    $event.preventDefault();
                };
                //---------------------------------------------------------------------
                // Watchers.
                //---------------------------------------------------------------------
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);