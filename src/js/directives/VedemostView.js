angular.module('vedemostView', ['leftSide'])
    .directive('vedemostView', ['$rootScope', 'VedemostService', function ($rootScope, VedemostService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                user: '=?',
                userFaculty: '=?',
                isFullAccess: '=?',
                isVisible: '=?'
            },
            template: '<div>\n    <div class="vedemost-left-side">\n        <left-side is-visible="isVisible" \n                   user="user" \n                   user-faculty="userFaculty"\n                ></left-side>\n    </div>\n    <div class="vedemost-right-side">\n        <table class="vedemost-main-table">\n            <tbody>\n            <tr>\n                <td class="vedemost-cl0"><span><strong>№</strong></span></td>\n                <td style="width: 50%;"><span><strong>Номи Фан</strong></span></td>\n                \n                <td style="width: 11%;text-align: center;"><span><strong>Амалиёт</strong></span></td>\n            </tr>\n            <tr ng-repeat="vedemost in vedemosts">\n                <td class="vedemost-cl0">{{$index+1}}</td>\n                <td style="width: 50%;" ng-bind="vedemost.subject_name + \'  (\' + vedemost.vd_num_ved +\')\'" ng-class="{\'open-vedemost\':vedemost.isOpened == 0}"></td>\n                <td style="width: 8%;text-align: center;">\n                    <span  class="vedemost-table-controls icon-pencil" ng-click="onLinkButtonClickHandler(vedemost)" ng-show="isFullAccess"></span>\n                    <a href="" class="vedemost-table-controls" ng-click="onEditButtonClickHandler(vedemost)">Донишҷуён</a>\n                </td>\n            </tr>\n            </tbody>\n        </table>\n    </div>\n</div>',
            link: function (scope) {
                var course, group, watchers = [], lastPolugodie = 1;
                scope.polugodie = 1;

                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }


                function onPolugodieChange(event, polugodie) {
                    lastPolugodie = scope.polugodie;
                    scope.polugodie = polugodie;
                    onTreeMenuSelectionChange(null, course, group);
                }

                function onTreeMenuSelectionChange(event, inCourse, inGroup) {
                    if (scope.isVisible && inCourse && inGroup) {
                        course = inCourse;
                        group = inGroup;
                        scope.vedemosts = null;
                        $rootScope.$$phase || scope.$apply();
                        VedemostService.getGroupVedemosts(course, group, scope.polugodie);
                    }
                }

                function onVedemostsLoaded() {
                    if (scope.isVisible) {
                        scope.vedemosts = VedemostService.vedemosts;
                        $rootScope.$$phase || scope.$apply();
                    }
                }


                function onIsVisibleChange(newValue, oldValue) {
                    if (newValue != oldValue) {
                        if (newValue) {
                            if (lastPolugodie != scope.polugodie) {
                                onTreeMenuSelectionChange(null, course, group);
                            }
                        }
                    }
                }

                scope.onEditButtonClickHandler = function (vedemost) {
                    $rootScope.$emit('showEditeVedemostView', vedemost);
                };

                scope.onLinkButtonClickHandler = function (vedemost) {
                    $rootScope.$emit('showVedemostLinkView', vedemost);
                };
                watchers.push(scope.$watch('isVisible', onIsVisibleChange));
                watchers.push($rootScope.$on('treeMenuSelectionChange', onTreeMenuSelectionChange));
                watchers.push($rootScope.$on('vedemostsLoaded', onVedemostsLoaded));
                watchers.push($rootScope.$on('polugodieChange', onPolugodieChange));
                watchers.push($rootScope.$on('$destroy', onDestroy));
            }
        }
    }]);