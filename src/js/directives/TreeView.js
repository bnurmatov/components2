angular.module('treeView', ['treeViewItem'])
    .directive("treeView", ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                treeModel: '=?',
                isVisible: '=?'
            },
            template: '<ul class="tree-view-menu"  ng-show="isVisible">\n    <tree-view-item ng-repeat="node in treeModel" menu-vo="node"></tree-view-item>\n</ul>',
            link: function (scope) {
                //---------------------------------------------------------
                // Private Variables
                //---------------------------------------------------------
                var watchers = [];
                //---------------------------------------------------------
                // Scope Variables
                //---------------------------------------------------------
                scope.isVisible = angular.isDefined(scope.isVisible) ? scope.isVisible : true;
                //---------------------------------------------------------
                // Private Methods
                //---------------------------------------------------------
                /**
                 *
                 * @param event
                 */
                function onDestroy() {
                    for (var key in scope) {
                        if (key[0] !== "$") {
                            scope[key] = null;
                        }
                    }

                    while (watchers.length > 0) {
                        watchers.pop()();
                    }

                    onDestroy = null;
                    watchers = null;
                }


                //---------------------------------------------------------
                // Public Methods
                //---------------------------------------------------------
                watchers.push(scope.$on('$destroy', onDestroy));
            }
        };
    }]);