'use strict';
angular.module('tService',
    [
    ])
    .service('tService',
        [
            '$rootScope',
            '$q',
            '$http',
            function ($rootScope, $q, $http) {

                //------------------------------------------------------------------------
                // Constants
                //------------------------------------------------------------------------
                //------------------------------------------------------------------------
                // Private variables
                //------------------------------------------------------------------------
                var service = this,
                    httpOptions = {
                        method: 'GET',
                        url: '',
                        responseType: 'json'
                    };
                //------------------------------------------------------------------------
                // Public variables
                //------------------------------------------------------------------------
                /**
                 *
                 * @type {null}
                 */
                this.config = null;
                this.baseAPIUrl = "http://asu.techuni.tj";
                this.localeProps = {};
                //------------------------------------------------------------------------
                // Functions(getters and setters)
                //------------------------------------------------------------------------
                /**
                 * check response on error
                 * @param data
                 * @returns {*|boolean}
                 */
                function isRequestDoneSuccesfuly(data) {
                    var isSuccess = (data && !data.error);
                    if (!isSuccess) {
                        service.spinError();
                        console.log("Something wrong: " + data.message);
                    } else {
                        service.spinEnd();
                    }
                    return isSuccess;
                }

                function loadVedmostsSuccess(response) {
                    if (isRequestDoneSuccesfuly(response[0].data) && isRequestDoneSuccesfuly(response[1].data)) {
                        var props = service.localeProps;
                        props.vedemosts = response[0].data.vedemosts;
                        service.serverDate = response[1].data.serverDate;
                        parseOrders(response[1].data.orders);
                    }
                }

                function loadOrdersSuccess(response) {
                    if (isRequestDoneSuccesfuly(response)) {
                        service.serverDate = response.data.serverDate;
                        parseOrders(response.data.orders);
                    }
                }

                function addOrderSuccess(response) {
                    if (isRequestDoneSuccesfuly(response)) {
                        parseOrders(response.orders);
                    }
                }

                function loadStudentsSuccess(response) {
                    if (isRequestDoneSuccesfuly(response)) {
                        service.localeProps.students = response.students;
                    }
                }
                function loadStudentInfoSuccess(response) {
                    if (isRequestDoneSuccesfuly(response)) {
                        var props = service.localeProps;
                        props.studentInfo = response.student;
                        props.studentInfo.semestr = +((+(props.studentInfo.Course) - 1 ) * 2) + (+props.order.polyear)
                    }
                }

                function parseOrder(order){
                    var arr = [],
                        veds = order.vedemosts.split(';'),
                        summa = 0,
                        totalMark,
                        ved,
                        vdIds = [],
                        vedProps,
                        title = '',
                        len = veds.length;
                    for (var i = 0; i < len; i++) {
                        vedProps = veds[i].split('#');
                        title += vedProps[1] + ", " ;
                        totalMark = +vedProps[3];
                        if(service.config.uchebYear >= 16) {
                            summa += (totalMark < 45 ? +vedProps[2] : 0) * (+order.price);
                        } else {
                            summa += +vedProps[2] * (+order.price);
                        }
                        vdIds.push(vedProps[0]);
                        ved = {vd_id:vedProps[0], subject:vedProps[1], totalCredits:vedProps[2], totalMark:totalMark};
                        arr.push(ved);
                    }
                    order.vedemosts = arr;
                    if(summa == summa.toFixed(2)){
                        order.summa = summa;
                    } else {
                        order.summa = (Math.ceil(summa * 10) / 10).toFixed(2);
                    }
                    order.title = title;
                    order.vdIds = vdIds;
                    order.uchebYearStr = "20" + order.uchebyear + "-20" + (+order.uchebyear + 1);
                    order.creationTime = order.creationTime.split(" ")[0].split('-').reverse().join('-');
                    order.toDayInvoiceGenerated = service.serverDate == order.generate_invoice_date;
                    return order;
                }

                function parseOrders(orders){
                    var arr = [],
                        len = orders.length,
                        props = service.localeProps;
                    for (var i = 0; i < len; i++) {
                        arr.push(parseOrder(orders[i]));
                    }
                    props.orders = arr;
                    parseVedemostsDisableState();
                }

                function parseVedemostsDisableState(){
                    var orders,
                        len ,
                        props = service.localeProps,
                        ved,
                        veds,
                        vdIds = [];
                    if(props.vedemosts && props.orders) {
                        orders = props.orders;
                        len = orders.length;
                        for (var i = 0; i < len; i++) {
                            vdIds = vdIds.concat(orders[i].vdIds);
                        }
                        veds = props.vedemosts;
                        len = veds.length;
                        for (var i = 0; i < len; i++) {
                            ved = veds[i];
                            ved.selected = false;
                            ved.disable = vdIds.indexOf(ved.vd_id) !== -1;
                        }
                    }
                }

                function requestError(response, status, headers, config) {
                    throw new Error("Something wrong: " + response);
                    console.log(response);
                    service.spinError();
                }


                //------------------------------------------------------------------------
                // Private Methods
                //------------------------------------------------------------------------


                //------------------------------------------------------------------------
                // Public Methods
                //------------------------------------------------------------------------
                /**
                 *
                 * @param config
                 */
                this.initConfig = function (config) {
                    if (config) {
                        var props = service.localeProps;
                        service.config = config;
                        props.userId = parseInt(config.teacherId, 10) || -1;
                        props.userFaculty = config.teacherFaculty;
                        httpOptions.headers = httpOptions.headers || {};
                        httpOptions.headers.user = props.userId;
                        props.teacherName = config.teacherName;
                        props.teacherType = config.teacherType;
                        props.teacherTime = config.teacherTime;
                    }
                };
                /**
                 * Show Busy indicator
                 */
                this.spinStart = function (){
                    $rootScope.$emit('spinStart');
                };
                /**
                 * Hide Busy indicator
                 */
                this.spinEnd = function (){
                    $rootScope.$emit('spinEnd');
                };
                /**
                 * Show error message
                 */
                this.spinError = function (){
                    $rootScope.$emit('spinError');
                };
                /**
                 * Authenticate user on server and return user vo
                 * @param login
                 * @param password
                 */
                this.getVedemostsByStudentId = function (studentId) {
                    var promises = [];
                    service.spinStart();
                    studentId = studentId.replace("/", "");
                    httpOptions.method = "GET";
                    httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getStudentTrimestrVedemosts/' + studentId;
                    promises.push($http(httpOptions));
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=getStudentOrders/' + studentId;
                    promises.push($http(httpOptions));
                    $q.all(promises).then(loadVedmostsSuccess)
                        .catch(requestError);
                };
                /**
                 * Authenticate user on server and return user vo
                 * @param login
                 * @param password
                 */
                this.getOrdersByStatus = function (status) {
                    service.spinStart();
                    httpOptions.method = "GET";
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=getOrdersByStatus/' + status;
                    $http(httpOptions)
                        .then(loadOrdersSuccess, requestError);
                };
                /**
                 * Authenticate user on server and return user vo
                 * @param login
                 * @param password
                 */
                this.getGroupStudents = function (course, group) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getTrimestrStudentsByCourseAndGroup/' + course + '/' + group.replace('/','$_$');
                    httpOptions.method = "GET";
                    $http(httpOptions)
                        .success(loadStudentsSuccess)
                        .error(requestError);
                };
                /**
                 * Authenticate user on server and return user vo
                 * @param login
                 * @param password
                 */
                this.getStudentInfo = function (studentId) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/examination/ExaminationAPI.php?request=getStudentInfo/' + studentId.replace('/','');
                    httpOptions.method = "GET";
                    $http(httpOptions)
                        .success(loadStudentInfoSuccess)
                        .error(requestError);
                };

                this.generateInvoice = function(order){
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=generateInvoice';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify(order);
                    return  $http(httpOptions);
                };

                this.addNewOrder = function (order) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=addOrder';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify(order);
                    $http(httpOptions)
                        .success(addOrderSuccess)
                        .error(requestError);
                };
                /**
                 *
                 * @param order
                 * @returns {*}
                 */
                this.updateOrder = function (order) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=updateOrder';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify(order);
                    return $http(httpOptions);
                };
                /**
                 *
                 * @param order
                 * @returns {*}
                 */
                this.updateOrderStatus = function (orderId, status) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=updateOrderStatus';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify({orderId:orderId, status:status});
                    return $http(httpOptions);
                };
                /**
                 *
                 * @param order
                 * @returns {*}
                 */
                this.onUpdateConcessionClicked = function (orderId, concessionStatus) {
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=updateOrderConcessionStatus';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify({orderId:orderId, concessionStatus:concessionStatus});
                    return $http(httpOptions);
                };
                /**
                 * Remove selected order
                 */
                this.removeOrder = function(order){
                    service.spinStart();
                    httpOptions.url = service.baseAPIUrl + '/api/v1/order/TrimesterOrderAPI.php?request=removeOrder';
                    httpOptions.method = "POST";
                    httpOptions.headers['Content-Type'] = 'text/plain';
                    httpOptions.data = JSON.stringify({'orderId':order.id});
                    $http(httpOptions)
                        .success(function(){
                            service.spinEnd();
                            var orders = service.localeProps.orders,
                                len = orders.length;
                            for (var i = 0; i < len; i++) {
                                if(orders[i].id === order.id){
                                    orders.splice(i, 1);
                                    break;
                                }
                            }
                            parseVedemostsDisableState();
                        })
                        .error(requestError);
                };
            }]);