angular.module('appealService', [])
    .service('appealService', ['$rootScope', '$http', '$sce', '$timeout', function ($rootScope, $http, $sce, $timeout) {

        //-----------------------------------------------------------------------------
        // Private Variables
        //-----------------------------------------------------------------------------

        var service = this;
        var questionType = {

            /**
             * одиночный выбор (single choice)
             */
            '1': 'single_choice',

            /**
             * множественный выбор (multiple choice)
             */
            '2': 'multiple_choise',

            /**
             * установление соответствия (establishment of conformity)
             */
            '3': 'manual_enter_text',

            /**
             * указание истиности или ложности (indication of the truth or falsity)
             */
            '4': 'manual_enter_number',

            /**
             * ручной ввод числа (manual enter number)
             */
            '5': 'establish_conformity',

            /**
             * ручной ввод текста (manual enter text)
             */
            '6': 'indication_true_false',

            /**
             * заполнение пропустов (filling the gaps)
             */
            '7': 'filling_the_gaps',

            /**
             * одиночный выбор (single choice)
             */
            'single_choice': 1,

            /**
             * множественный выбор (multiple choice)
             */
            'multiple_choise': 2,

            /**
             * установление соответствия (establishment of conformity)
             */
            'manual_enter_text': 3,

            /**
             * указание истиности или ложности (indication of the truth or falsity)
             */
            'manual_enter_number': 4,

            /**
             * ручной ввод числа (manual enter number)
             */
            'establish_conformity': 5,

            /**
             * ручной ввод текста (manual enter text)
             */
            'indication_true_false': 6,

            /**
             * заполнение пропустов (filling the gaps)
             */
            'filling_the_gaps': 7
        };
        //-----------------------------------------------------------------------------
        // Public Variables
        //-----------------------------------------------------------------------------

        /**
         *
         * @type {null}
         */
        this.config = null;

        /**
         *
         * @type {null}
         */
        this.currentVedemost = null;

        /**
         *
         * @type {null}
         */
        this.authorizeInfo = null;

        /**
         *
         * @type {null}
         */
        this.originalQuestions = null;

        /**
         *
         * @type {null}
         */
        this.userAnsweredQuestions = null;

        /**
         *
         * @type {Array}
         */
        this.examinationTypes = [];
        /**
         *
         * @type {null}
         */
        this.userName = "";
        /**
         *
         * @type {null}
         */
        this.student = null;

        this.studentId = null;
        /**
         *
         * @type {string}
         */
        this.baseAPIUrl = "http://asu.techuni.tj";

        /**
         *
         * @type {string}
         */
        this.userAnswer = '';

        this.httpOptions = {
            method: 'GET',
            url: '',
            responseType: 'json'
        };

        this.vedemostResult = null;

        this.totalCorrects = 0;
        this.polugodie = 1;
        //-----------------------------------------------------------------------------
        // Private Functions
        //-----------------------------------------------------------------------------

        function getTotalCorrects(questions) {
            var total = 0;
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].type == 5 || questions[i].type == 6 || questions[i].type == 7) {
                    total += questions[i].answers.length;
                } else if (questions[i].type !== 3 && questions[i].type !== 4) {
                    for (var j = 0; j < questions[i].answers.length; j++) {
                        if (questions[i].answers[j].isCorrect) {
                            total++;
                        }
                    }
                } else {
                    total++;
                }
            }
            return total;
        }

        function getTotalCorrects2(questions) {
            var total = 0;
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].type == QuestionTypes.ESTABLISH_CONFORMITY || questions[i].type == QuestionTypes.INDICATION_TRUE_FALSE || questions[i].type == QuestionTypes.FILLING_THE_GAPS) {
                    total += questions[i].answers.length;
                } else if (questions[i].type !== QuestionTypes.MANUAL_ENTER_TEXT && questions[i].type !== QuestionTypes.MANUAL_ENTER_NUMBER) {
                    for (var j = 0; j < questions[i].answers.length; j++) {
                        if (questions[i].answers[j].isCorrect) {
                            total++;
                        }
                    }
                } else {
                    total++;
                }
            }
            return total;
        }

        function exam1Filter(item) {
            return item.exam_type == 1;
        }

        function exam2Filter(item) {
            return item.exam_type == 2;
        }

        function exam3Filter(item) {
            return item.exam_type == 3;
        }

        function exam4Filter(item) {
            return item.exam_type == 4;
        }

        function exam5Filter(item) {
            return item.exam_type == 5;
        }

        function filterVariantsByTypes(items) {
            var variants = [];
            variants[1] = items.filter(exam1Filter);
            variants[2] = items.filter(exam2Filter);
            variants[3] = items.filter(exam3Filter);
            variants[4] = items.filter(exam4Filter);
            variants[5] = items.filter(exam5Filter);
            return variants;
        }

        function onInfoLoadSuccess(response) {
            if (isRequestDoneSuccessfuly(response)) {
                service.student = response.student;
            } else {
                console.log('Authorization failed');
            }
        }

        function onInfoLoadError(error) {
            console.log('Authorization failed');
        }

        function onDestroy() {
            unDestroy();
            unDestroy = null;
            onDestroy = null;
        }

        function onVariantsLoadSuccess(response) {
            if (isRequestDoneSuccessfuly(response)) {
                var variants = filterVariantsByTypes(response.variants);
                service.examinationTypes = [];
                var examtype;
                for (var i = 0; i < 5; i++) {
                    if (variants[i + 1].length > 0) {
                        examtype = new ExamTypeVo();
                        examtype.id = i + 1;
                        examtype.name = ExaminationTypes.getExaminationType(i + 1);
                        examtype.variants = variants[i + 1];
                        service.examinationTypes.push(examtype);
                    }
                }
                $rootScope.$emit('variantsLoaded');
            } else {
                console.log('Multi session');
                $rootScope.$emit('multiSession');
            }
        }

        function onVariantsLoadError(errorString) {
            service.spinError();
            console.log('Multi session');
            $rootScope.$emit('multiSession');
        }

        function getQuestionById(number) {
            for (var i = 0; i < service.originalQuestions.length; i++) {
                if (service.originalQuestions[i].id === number) {
                    return service.originalQuestions[i];
                }
            }
            return {};
        }

        function parseUserQuestion(question) {
            var ques = question.split('$');
            var originQuestion = getQuestionById(parseInt(ques[0], 10));
            if (!originQuestion) {
                return null;
            }
            var userQst;
            switch (originQuestion.type) {
                case QuestionTypes.SINGLE_CHOICE:
                    userQst = AppealQuestionParser.parseSingleQuestion(originQuestion, question);
                    break;
                case QuestionTypes.MULTIPLE_CHOICE:
                    userQst = AppealQuestionParser.parseMultipleQuestion(originQuestion, question);
                    break;
                case QuestionTypes.INDICATION_TRUE_FALSE:
                    userQst = AppealQuestionParser.parseIndicationQuestion(originQuestion, question);
                    break;
                case QuestionTypes.ESTABLISH_CONFORMITY:
                    userQst = AppealQuestionParser.parseEstablishQuestion(originQuestion, question);
                    break;
                case QuestionTypes.MANUAL_ENTER_TEXT:
                    userQst = AppealQuestionParser.parseEnterTextQuestion(originQuestion, question);
                    break;
                case QuestionTypes.MANUAL_ENTER_NUMBER:
                    userQst = AppealQuestionParser.parseEnterNumberQuestion(originQuestion, question);
                    break;
                case QuestionTypes.FILLING_THE_GAPS:
                    userQst = AppealQuestionParser.parseFillingQuestion(originQuestion, question);
                    break;
            }
            return userQst;
        }

        function parseUserAnsweredQuestions(userAnswer) {
            var arr = [];
            var userQuestions = userAnswer.split('$$');
            var userQuestion;
            for (var i = 0; i < userQuestions.length; i++) {
                userQuestion = parseUserQuestion(userQuestions[i]);
                if (userQuestion) {
                    userQuestion.position = (i + 1);
                    arr.push(userQuestion);
                }
            }
            return arr;
        }

        function parseQuestions(allQuestions) {
            var arr = [],
                parsedQuestion,
                question,
                questionsLen = allQuestions.length;

            for (var i = 0; i < questionsLen; i++) {
                question = allQuestions[i];
                switch (question.qs_type) {
                    case QuestionTypes.SINGLE_CHOICE:
                        parsedQuestion = QuestionParserUtil.parseSingle($sce, question);
                        break;
                    case QuestionTypes.MULTIPLE_CHOICE:
                        parsedQuestion = QuestionParserUtil.parseMultiple($sce, question);
                        break;
                    case QuestionTypes.ESTABLISH_CONFORMITY:
                        parsedQuestion = QuestionParserUtil.parseEstablish($sce, question);
                        break;
                    case QuestionTypes.INDICATION_TRUE_FALSE:
                        parsedQuestion = QuestionParserUtil.parseIndication($sce, question);
                        break;
                    case QuestionTypes.MANUAL_ENTER_NUMBER:
                        parsedQuestion = QuestionParserUtil.parseEnterNumber($sce, question);
                        break;
                    case QuestionTypes.MANUAL_ENTER_TEXT:
                        parsedQuestion = QuestionParserUtil.parseEnterText($sce, question);
                        break;
                    case QuestionTypes.FILLING_THE_GAPS:
                        parsedQuestion = QuestionParserUtil.parseFillingGaps(question);
                        break;
                }
                arr.push(parsedQuestion);
            }
            return arr;
        }

        function onLoadTestResultSuccess(response) {
            if (isRequestDoneSuccessfuly(response)) {
                service.originalQuestions = parseQuestions(QuestionParserUtil.parseQuestions(response.questions));
                service.totalCorrects = getTotalCorrects(service.originalQuestions);
                service.userAnsweredQuestions = parseUserAnsweredQuestions(service.userAnswer);
                $rootScope.$emit('testResultLoaded');
            }
        }

        function isRequestDoneSuccessfuly(data) {
            var isSuccess = (data && !data.error);
            if (!isSuccess) {
                service.spinError();
                console.log("Something wrong: " + data.message);
            } else {
                service.spinEnd();
            }
            return isSuccess;
        }

        function getStudentInfo(userId) {
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/examination/ExaminationAPI.php?request=getStudentInfo/' + userId;
            service.httpOptions.method = "GET";
            $http(service.httpOptions)
                .success(onInfoLoadSuccess)
                .error(requestError);
        }

        function onExamResultLoadSuccess(response) {
            if (isRequestDoneSuccessfuly(response)) {
                service.vedemostResult = response.vedemost;
                $rootScope.$emit('vedemostResultLoaded');
            }
        }

        function onExamResultLoadError() {

        }

        function getStudentExamResults(vedemostId, userId) {
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getPodvedemostById/' + vedemostId + '/' + userId;
            service.httpOptions.method = "GET";
            $http(service.httpOptions)
                .success(onExamResultLoadSuccess)
                .error(requestError);
        }

        function onUpdateScoreSuccess(responce) {
            if (isRequestDoneSuccessfuly(responce)) {
                $rootScope.$emit('appealResultUpdated');
            }
        }


        function requestError(response, status, headers, config) {
            throw new Error("Something wrong: " + response);
            console.log(response);
            service.spinError();
        }

        //-----------------------------------------------------------------------------
        // Public Functions
        //-----------------------------------------------------------------------------
        /**
         * Show Busy indicator
         */
        this.spinStart = function () {
            $rootScope.$emit('spinStart');
        };
        /**
         * Hide Busy indicator
         */
        this.spinEnd = function () {
            $rootScope.$emit('spinEnd');
        };
        /**
         * Show error message
         */
        this.spinError = function () {
            $rootScope.$emit('spinError');
        };

        this.updateUserAppealScore = function (comment, score) {
            var obj = {
                vedemostId: service.currentVedemost.vedemost_id,
                studentId: service.studentId,
                examType: +(service.currentVedemost.exam_type),
                score: score,
                comment: comment + "( " + service.userName + ")"
            };
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateStudentAppealResult';
            service.httpOptions.method = 'POST';
            service.httpOptions.data = JSON.stringify(obj);
            service.httpOptions.headers['Content-Type'] = 'text/plain';
            $http(service.httpOptions)
                .success(onUpdateScoreSuccess)
                .error(requestError);
        };

        this.initConfig = function (data) {
            if (data) {
                service.studentId = data[1];
                service.locale = 'ru_RU';
                getStudentInfo(service.studentId.replace('/', ""));
            }
        };


        this.getStudentVariants = function (studentId, polugodie) {
            service.spinStart();
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/examination/ExaminationAPI.php?request=getVariantsByStudentIdAndPolugodie/' + studentId + "/" + polugodie;
            service.httpOptions.method = "GET";
            $http(service.httpOptions)
                .success(onVariantsLoadSuccess)
                .error(onVariantsLoadError);
        };

        this.loadVariantQuestions = function (vedemost) {
            service.spinStart();
            service.currentVedemost = vedemost;
            service.userAnswer = vedemost.user_answers;
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/question/QuestionAPI.php?request=getQuestionsByIds/' + vedemost.questions;
            service.httpOptions.method = 'GET';
            $http(service.httpOptions)
                .success(onLoadTestResultSuccess)
                .error(requestError);
            getStudentExamResults(vedemost.vedemost_id, service.studentId.replace("/", ""));
        };

        this.loadTeacherVariantQuestions = function (teacherId, testId) {
            service.httpOptions.url = service.baseAPIUrl + '/api/v1/examination/ExaminationAPI.php?request=getTeacherAppeal/' + teacherId + '/' + testId;
            service.httpOptions.method = 'GET';
            return $http(service.httpOptions)
                .then(function (response) {
                    response = response.data || response;
                    if (isRequestDoneSuccessfuly(response)) {
                        var questions = QuestionParserUtil.parseQuestions(response.questions);
                        questions.map(function (question) {
                            question.qs_type = questionType['' + question.qs_type];
                            return question;
                        });
                        service.originalQuestions = parseQuestions(questions);
                        service.totalCorrects = getTotalCorrects2(service.originalQuestions);
                        service.userAnsweredQuestions = parseUserAnsweredQuestions(response.variant.user_answers);
                        return service.userAnsweredQuestions;
                    }
                    else {
                        throw new Error('loadTeacherVariantQuestions');
                    }
                }
            ).catch(requestError);

        };

        //-----------------------------------------------------------------------------
        // Watchers (Event Listeners)
        //-----------------------------------------------------------------------------

        var unDestroy = $rootScope.$on('$destroy', onDestroy);

    }]);
