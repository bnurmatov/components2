angular.module('VedemostService', [])
    .service('VedemostService', ['$rootScope', '$http', function ($rootScope, $http) {
        'use strict';
        //------------------------------------------------------------------------
        // Constants
        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        // Private variables
        //------------------------------------------------------------------------
        var service = this,
            httpOptions = {
                method: 'GET',
                url: '',
                responseType: 'json'
            };
        //------------------------------------------------------------------------
        // Public variables
        //------------------------------------------------------------------------

        /**
         * Id of test
         */
        this.teacherId;
        /**
         * Id of test
         */
        this.teacherName;

        /**
         * Id of test
         */
        this.teacherTime;
        /**
         * Id of test
         */
        this.teacherType;

        this.profiles = null;

        this.tests;

        this.baseAPIUrl="http://asu.techuni.tj";
        //------------------------------------------------------------------------
        // Functions(getters and setters)
        //------------------------------------------------------------------------
        /**
         * check response on error
         * @param data
         * @returns {*|boolean}
         */
        function isRequestDoneSuccesfuly(data) {
            var isSuccess = (data && !data.error);
            if (!isSuccess) {
                service.spinError();
                console.log("Something wrong: " + data.message);
            } else {
                service.spinEnd();
            }
            return isSuccess;
        }


        function parseTest(item){
            var vo = new TestVO();
            vo.test_id = parseInt(item.test_id, 10);
            vo.teacher_id = parseInt(item.teacher_id, 10);
            vo.teacher_name = item.teacher_name;
            vo.subject_id = parseInt(item.subject_id, 10);
            vo.subject_name = item.subject_name;
            vo.kafedra_id = parseInt(item.kafedra_id, 10);
            vo.kafedra_name = item.kafedra_name;
            vo.polugodie = parseInt(item.polugodie,10);
            vo.description = item.description;
            vo.language = item.language;
            vo.question_count = parseInt(item.question_count, 10);
            vo.locked = parseInt(item.locked, 10) != 0;
            return vo;
        }

        function parseTests(tests){
            var len = tests.length,
                arr = [];
            for (var i = 0; i < len; i++) {
                arr.push(parseTest(tests[i]));
            }
            return arr;
        }


        function loadTestssSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.tests = parseTests(response.tests);
                service.isDataLoaded = true;
                $rootScope.$emit('testsLoaded');
            } else {
                console.log('failed');
            }
        }

        function profilesLodedSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.profiles = response.profiles;
                $rootScope.$emit('profilesLoaded');
            } else {
                console.log('Get profiles failed');
            }
        }

        function lockUnlockTestsSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('testLockStateUpdated');
            } else {
                console.log('failed');
            }
        }

        function loadGroupVedemostsSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.vedemosts = response.vedemosts;
                $rootScope.$emit('vedemostsLoaded');
            } else {
                console.log('failed');
            }
        }
        function loadPodvedemostsSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.podvedemost = response.vedemost;
                $rootScope.$emit('podvedemostLoaded');
            } else {
                console.log('failed');
            }
        }

        function loadGroupStudentsSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                service.students = response.students;
                $rootScope.$emit('studentsLoaded');
            } else {
                console.log('failed');
            }
        }
        function updateStudentStatusSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('studentStatusUpdated');
            } else {
                console.log('failed');
            }
        }

        function updateVedemostTestInfoSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('vedemostTestInfoUpdated');
            } else {
                console.log('failed');
            }
        }
        function updatePodvedemostSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('podvedemostUpdated');
            } else {
                console.log('failed');
            }
        }
        function removeStudentVariantSuccess(response) {
            if (isRequestDoneSuccesfuly(response)) {
                $rootScope.$emit('studentVariantRemoved');
            } else {
                console.log('failed');
            }
        }

        function requestError(response, status, headers, config) {
            throw new Error("Something wrong: " + response);
            console.log(response);
            service.spinError();
        }

        //------------------------------------------------------------------------
        // Private Methods
        //------------------------------------------------------------------------


        //------------------------------------------------------------------------
        // Public Methods
        //------------------------------------------------------------------------
        /**
         * Show Busy indicator
         */
        this.spinStart = function (){
            $rootScope.$emit('spinStart');
        };
        /**
         * Hide Busy indicator
         */
        this.spinEnd = function (){
            $rootScope.$emit('spinEnd');
        };
        /**
         * Show error message
         */
        this.spinError = function (){
            $rootScope.$emit('spinError');
        };

        this.initConfig = function (teacherId, teacherFaculty, teacherName) {
            if (teacherId) {
                service.teacherId = parseInt(teacherId, 10);
                httpOptions.headers = httpOptions.headers || {};
                httpOptions.headers.user = teacherId;
                service.teacherName = teacherName;
                service.teacherFaculty = teacherFaculty;
            }
        };

        /**
         * Authenticate user on server and return user vo
         * @param login
         * @param password
         */
        this.getTestsByKafedraId = function (kafedraId, polugodie) {
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/test/TestsAPI.php?request=getTestsByKafedraIdAndPolugodie/' + kafedraId + '/' + polugodie;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadTestssSuccess)
                .error(requestError);
        };

        /**
         * Delete question by id
         * @param questionId
         */
        this.getAllProfiles = function(){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/profile/ProfileAPI.php?request=getAllProfiles';
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(profilesLodedSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getGroupVedemosts = function(course, group, polugodie){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getVedemostsByCourseAndGroupAndPolugodie/' + course +'/'+ group.replace('/','$_$') + '/' + polugodie;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadGroupVedemostsSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getGroupAllVedemosts = function(course, group, polugodie){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getAllVedemostsByCourseAndGroupAndPolugodie/' + course +'/'+ group.replace('/','$_$') + '/' + polugodie;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadGroupVedemostsSuccess)
                .error(requestError);
        };

        /**
         * Delete question by id
         * @param questionId
         */
        this.getPodvedemostById = function(vdemostId, studentId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getPodvedemostById/' + vdemostId +'/'+ studentId.replace('/','');
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadPodvedemostsSuccess)
                .error(requestError);
        };

        /**
         * Delete question by id
         * @param questionId
         */
        this.getStudentsByVedemostId = function(vedemostId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getStudentsByVedemostId/' + vedemostId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadGroupStudentsSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.getTrimesterStudentsByVedemostId = function(vedemostId){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=getTrimesterStudentsByVedemostId/' + vedemostId;
            httpOptions.method = "GET";
            $http(httpOptions)
                .success(loadGroupStudentsSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.updateStudentVedemostStatus = function(vedemostId, studentId, status, exam_type, vedType){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateStudentVedemostStatus';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify({'vedemostId':vedemostId, 'studentId':studentId, 'status':status, 'exam_type':exam_type, 'vedType':vedType});
            $http(httpOptions)
                .success(updateStudentStatusSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.updateVedemostTestInfo = function(vedemost){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateVedemostTestInfo';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify(vedemost);
            $http(httpOptions)
                .success(updateVedemostTestInfoSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.updateStudentPodved = function(podvedemost){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=updateStudentPodved';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify(podvedemost);
            $http(httpOptions)
                .success(updatePodvedemostSuccess)
                .error(requestError);
        };
        /**
         * Delete question by id
         * @param questionId
         */
        this.removeStudentVariant = function(studentId, vedemostId, examType, polugodie, vedType){
            service.spinStart();
            httpOptions.url = service.baseAPIUrl + '/api/v1/vedemost/VedemostAPI.php?request=removeStudentVariant2';
            httpOptions.method = "POST";
            httpOptions.headers['Content-Type']= 'text/plain';
            httpOptions.data = JSON.stringify({'studentId':studentId, 'vedemostId':vedemostId, 'examType':examType, 'polugodie': polugodie, 'vedType': vedType});
            $http(httpOptions)
                .success(removeStudentVariantSuccess)
                .error(requestError);
        };

    }]);