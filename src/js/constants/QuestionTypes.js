var QuestionTypes = {

    /**
     * одиночный выбор (single choice)
     */
    SINGLE_CHOICE: 1,

    /**
     * множественный выбор (multiple choice)
     */
    MULTIPLE_CHOICE: 2,

    /**
     * ручной ввод текста (manual enter text)
     */
    MANUAL_ENTER_TEXT: 3,

    /**
     * ручной ввод числа (manual enter number)
     */
    MANUAL_ENTER_NUMBER: 4,

    /**
     * установление соответствия (establishment of conformity)
     */
    ESTABLISH_CONFORMITY: 5,

    /**
     * указание истиности или ложности (indication of the truth or falsity)
     */
    INDICATION_TRUE_FALSE: 6,

    /**
     * заполнение пропустов (filling the gaps)
     */
    FILLING_THE_GAPS: 7
};